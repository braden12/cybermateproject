//extend default javascript Error() to include a message

class CustomAPIERROR extends Error {
  constructor(message) {
    super(message);
  }
}

export default CustomAPIERROR;
