import { StatusCodes } from "http-status-codes" // an api for converting number codes to human-readable text
import CustomAPIERROR from "./custom-api.js" //extend default javascript Error() to include a message

class NotFoundError extends CustomAPIERROR {
  //extends CustomerAPIERROR to include a hardcoded status code
  constructor(message) {
    super(message);
    this.StatusCode = StatusCodes.NOT_FOUND;
  }
}

export default NotFoundError;
