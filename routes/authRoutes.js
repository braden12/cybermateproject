import express from "express";
const router = express.Router();
import auth from "../middleware/auth.js";
import {
  signup,
  activate,
  login,
  updateUser,
  forgot,
  reset,
  updateEmail,
  activateEmail,
} from "../controllers/authController.js";

router.route("/signup").post(signup);
router.route("/activate").post(activate);
router.route("/login").post(login);
router.route("/updateUser").patch(auth, updateUser);
router.route("/forgot_pass").post(forgot);
router.route("/reset_pass").post(auth, reset);
router.route("/update_email").post(updateEmail);
router.route("/gg-email").post(activateEmail);

export default router;
