import jwt from "jsonwebtoken"; // JSON Web Token generation package for authentication

const auth = (req, res, next) => {
  try {
    //check ac token
    //console.log(req.header("Authorization"))
    const token = req.header("Authorization");
    if (!token) return res.status(400).json({ msg: "Authentication failed" });

    //validate
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
      if (err) return res.status(400).json({ msg: "Authentication failed" });
      //success
      req.user = user;
      next(); //if the auth middleware function passes, call the next function
    });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

export default auth;
