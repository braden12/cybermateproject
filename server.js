import express from "express";
import dotenv from "dotenv";
dotenv.config();
import "express-async-errors";
import morgan from "morgan";
const app = express();

// db and authenticateUser
import connectDB from "./db/connect.js";

//routes
import authRouter from "./routes/authRoutes.js"

//middleware
import notFoundMiddleware from "./middleware/not-found.js";
import errorHandleMiddleware from "./middleware/error-handler.js";

if (process.env.NODE_ENV !== "production") {
  app.use(morgan("dev"));
}
app.use(express.json()) // inform the application we will be using json

app.get("/", (req, res) => {
  //throw new Error('Error')
  res.send("Welcome!");
});

app.get("/api/v1", (req, res) => {
  //throw new Error('Error')
  res.json({ msg: "API" });
});

app.use("/api/v1/auth", authRouter); //sets up authorisation at this path

app.use(notFoundMiddleware);
app.use(errorHandleMiddleware);
const port = process.env.PORT || 5000; //sets up port 5000

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL);
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}...`);
    });
  } catch (error) {
    console.log(error);
  }
};

start();
