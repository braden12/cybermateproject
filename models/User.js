import mongoose from "mongoose";
import validator from "validator"; //an installed validator package
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const UserSchema = new mongoose.Schema(
  {
    fname: {
      type: String,
      required: [true, "Please provide first name"],
      minlength: 2,
      maxlength: 20,
      trim: true,
    },
    lname: {
      type: String,
      required: [true, "Please provide last name"], //message given if field not provided
      minlength: 2,
      maxlength: 20,
      trim: true,
    },
    email: {
      type: String,
      required: [true, "Please provide email"],
      validate: {
        //use of the validator package isEmail function
        validator: validator.isEmail,
        message: "Please provide a valid email",
      },
      unique: true,
    },
    password: {
      type: String,
      required: [true, "Please provide password"],
      minlength: 8,
      select: false,
    },
  },
  { versionKey: false }
);

/*
UserSchema.pre('save', async function(){ //middleware hook triggered by UserModel.save()
    const salt = await bcrypt.genSalt(10)  //10 rounds of salt generation
    this.password = await bcrypt.hash(this.password, salt)
}) */

UserSchema.methods.createJWT = function () {
  return jwt.sign({ userId: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_LIFETIME,
  }); //process.env gets value from env
};

UserSchema.methods.comparePassword = async function (candidatePassword) {
  const isMatch = await bcrypt.compare(candidatePassword, this.password);
  return isMatch;
};

const UserModel = mongoose.model("users", UserSchema); //create model UserSchema for the users collection in MongoDB.
export default UserModel;
