import { json } from "express"; // allows the use of json
import UserModel from "../models/User.js"; // the defined user model for registration
import { StatusCodes } from "http-status-codes"; // gives human readable output to HTTP response codes
import { BadRequestError, UnAuthenticatedError } from "../errors/index.js"; // errors defined by us to be thrown when issues occur
import jwt from "jsonwebtoken"; // JSON Web Token generation package for authentication
import sendMail from "../helper/sendMail.js"; //
import createToken from "../helper/createToken.js";
import bcrypt from "bcryptjs";

const signup = async (req, res) => {
  //renamed from register
  const { fname, lname, email, password } = req.body;

  // check user has entered all the correct fields
  if (!fname || !lname || !email || !password) {
    throw new BadRequestError("please provide all values"); //express-async-error function
  }

  //checks if user already exists
  const userAlreadyExists = await UserModel.findOne({ email });
  if (userAlreadyExists) {
    throw new BadRequestError("Email already in use");
  }

  // hash password
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt);

  // create user object
  const newUser = { fname, lname, email, password: hashPassword };

  // create activation token using helper
  const activation_token = createToken.activation(newUser);

  // define the route for activation  CHANGE WITH HEROKU
  const url = `http://localhost:3000/api/v1/auth/activate/${activation_token}`;

  // send to given email(email) the authroute(url) and define the button
  sendMail.sendEmailRegister(email, url, "Verify your email");

  // respond a success (200) with message
  res.status(200).json({ msg: "Welcome! Please check your email." });
};

const activate = async (req, res) => {
  // called when POST to /api/v1/auth/activate
  try {
    // get token
    const { activation_token } = req.body;

    // verify token
    const user = jwt.verify(activation_token, process.env.JWT_SECRET);
    //console.log(user);
    const { fname, lname, email, password } = user;
    //console.log(fname, lname, email, password);
    // check if user email already exists
    const check = await UserModel.findOne({ email });
    if (check)
      return res.status(400).json({ msg: "This email is already registered." });

    // create a user via Mongoose in MongoDB
    const newUser = await UserModel.create({ fname, lname, email, password });

    // creating JSON Web Token
    const token = newUser.createJWT();

    // activation success
    res.status(StatusCodes.CREATED).json({
      user: {
        fname: newUser.fname,
        lname: newUser.lname,
        email: newUser.email,
      },
      token,
    });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

const login = async (req, res) => {
  // Takes the email and password from the JSON of the request
  const { email, password } = req.body;

  // If no email or password, throw a 400 error
  if (!email || !password) {
    throw new BadRequestError("Please provide all values");
  }

  // Check if user already exists in the MongoDB database
  const user = await UserModel.findOne({ email }).select("+password"); //added the +password since select:false on UserModel

  // if no user exists, throw a 401 unauthenticated error
  if (!user) {
    throw new UnAuthenticatedError("Invalid Credentials");
  }

  // Check if user password is matches with the recorded password
  const isPasswordCorrect = await user.comparePassword(password);

  // if the password doesn't match, throw 401 unauthenticated error
  if (!isPasswordCorrect) {
    throw new UnAuthenticatedError("Invalid Credentials");
  }

  // elsif the password matches, create a JSON Web Token using the user
  const token = user.createJWT();

  // reset the password of the user
  user.password = undefined; //set as undefined so when we get the response back we only get userdata but not the password hash as well.

  // respond with 200, and JSON containing the user and token
  res.status(StatusCodes.OK).json({ user, token });
};

const forgot = async (req, res) => {
  try {
    const { email } = req.body;

    //check email
    const user = await UserModel.findOne({ email });
    if (!user)
      return res
        .status(400)
        .json({ msg: "This email is not registered in our system." });

    //create ac token
    const activation_token = user.createJWT();

    //send email
    const url = `http://localhost:3000/auth/reset-password/${activation_token}`;
    const fname = user.fname;
    sendMail.sendEmailReset(email, url, "Reset your password", fname);

    //success
    res
      .status(200)
      .json({ msg: "Re-send the password, please check your email." });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

const reset = async (req, res) => {
  try {
    //get password
    const { password } = req.body;
    if (password.length < 8) {
      return res
        .status(400)
        .json({ msg: "Password must be 8 characters or more" });
    }
    //console.log(req)
    //hash password
    //console.log(password);
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    //console.log(hashPassword);
    //update password
    await UserModel.findOneAndUpdate(
      { _id: req.user.userId },
      { password: hashPassword }
      //{ returnDocument: 'after' | 'before' }
    );

    //reset success
    res.status(200).json({ msg: "Password was updated successfully." });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

const updateUser = async (req, res) => {
  try {
    //get info
    const { fname, lname, email } = req.body.currentUser;
    await UserModel.findOneAndUpdate(
      { _id: req.user.userId },
      { fname, lname, email }
    );
    res.status(200).json({ msg: "Updated successfully." });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

//update it with auth, change sendEmailReset func
const updateEmail = async (req, res) => {
  try {
    const { fname, old_email, new_email } = req.body;

    const user_info = { fname, old_email, new_email };

    const email = new_email;
    const userEmail = await UserModel.findOne({ email }); //check if new email is already in system

    if (!userEmail) {
      const activation_token = createToken.activation({ user_info });
      //console.log(activation_token);
      const url = `http://localhost:3000/api/v1/auth/gg-email/${activation_token}`;

      sendMail.sendEmailConfirmChange(
        old_email,
        url,
        "Verify your email",
        fname
      );

      // respond a success (200) with message
      res.status(200).json({ msg: "Welcome! Please check your email." });
    } else {
      return res
        .status(400)
        .json({ msg: "This email is already registered in our system." });
    }
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

const activateEmail = async (req, res) => {
  try {
    const { activation_token } = req.body;
    const user = jwt.verify(activation_token, process.env.JWT_SECRET);
    const old_email = user.user_info.old_email;
    const email = user.user_info.new_email;
    {
      await UserModel.findOneAndUpdate({ email: old_email }, { email });
    }
    res.status(200).json({ msg: email });
  } catch (err) {
    res.status(500).json({ msg: err.message });
  }
};

export {
  signup,
  activate,
  login,
  updateUser,
  forgot,
  reset,
  updateEmail,
  activateEmail,
}; //imported into authRoutes.js
