# CYBERMATE PROJECT

**A web platform for learning about cybersecurity**

### potential missing files

.env

**should contain:**

MONGO_URL='mongodb+srv://Scotty:H77Ka4f3LQpELg8H@cluster0.zxec7wk.mongodb.net/Mern?retryWrites=true&w=majority'

JWT_SECRET=RfUjWnZr4u7x!A%D\*G-KaPdSgVkYp2s5

JWT_LIFETIME=1d

G_CLIENT_ID=776866426487-rjboauovjqg2amdslpp5riovebvqaq6e.apps.googleusercontent.com
G_CLIENT_SECRET=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
G_REFRESH_TOKEN=1//04_qQWZBBaVxXCgYIARAAGAQSNwF-L9Irulaqk2wq8LCwVGgbAYfg_Re6oGvw5AoarcGe_1jIifnCLkCcjOZ2ReXBFWqQRPyqcxc
ADMIN_EMAIL=qwerteey1@gmail.com

**These will be changed/removed once the project moves into production**

### HOW TO START THE WEB APPLICATION

Create a terminal in the client folder of the project

Type **npm install** and hit enter

Create a terminal in the cybermate project

Type **npm install** and hit enter

Then **npm start** and hit enter

If successful you'll connect to Scotty's MongoDB cluster and a webpage on your browser

### Connect to MONGODB COMPASS

MongoDB Compass is a GUI tool to look at and handle the MongoDB database

This string (URI) is used to connect to MongoDB Compass to visualise the database.

**mongodb+srv://Scotty:H77Ka4f3LQpELg8H@cluster0.zxec7wk.mongodb.net/test**
