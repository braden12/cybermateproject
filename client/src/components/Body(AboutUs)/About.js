import { Component } from "react";
// import homebody.css
import "../Body(Home)/Homebody.css";

import { Link } from "react-router-dom";

class About extends Component {
  render() {
    return (
      <div className="MainScreen">
        <div className="Box">
          <h3>Why Cycademy?</h3>
          <p>
            Cycademy is a massive online cybersecurity training platform that
            allows individuals, companies, and other organizations to enhance
            their cybersecurity skills.
          </p>
          <div className="AboutUsTable">
            <table>
              <tr>
                <th>
                  <h3>Up to date training modules</h3>
                </th>
                <th>
                  <h3>Perfect for SMEs</h3>
                </th>
              </tr>
              <tr>
                <td>
                  <p>
                    Our training modules are updated regularly to ensure that
                    our users are always up to date with the latest
                    cybersecurity trends and threats.
                  </p>
                </td>
                <td>
                  <p>
                    Our lessons are designed to get businesses started on their
                    journey to securing their data and systems.
                  </p>
                </td>
              </tr>
              <tr>
                <th>
                  <h3>Learn the Fundamentals of Cybersecurity for Free!</h3>
                </th>
                <th>
                  <h3>Easy To Understand Modules</h3>
                </th>
              </tr>
              <tr>
                <td>
                  <p>
                    All of our training modules are free to access a click away.
                    All you have to do is make an account by clicking {""}
                    <Link to="/signup" className="member-btn">
                      HERE
                    </Link>
                  </p>
                </td>
                <td>
                  <p>
                    Our training modules were designed to be easy to understand
                    so that everyone can learn the fundamentals of
                    cybersecurity.
                  </p>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
export default About;
