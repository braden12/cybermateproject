import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import "./Navbar.css";
import { Link } from "react-router-dom";
import logo from "../../assets/images/Logo.png";
import { MenuItems } from "./Menuitem";

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1, position: "sticky", top: "0px" }}>
      <AppBar
        position="static"
        style={{
          color: "black",
          backgroundColor: "white",
          alignItems: "center",
          marginTop: "-1%",
        }}
      >
        <Toolbar>
          <Link to="/">
            <img className="Newnavbar-logo" src={logo} alt="CyberMate" />
          </Link>
          <ul>
            {MenuItems.map((item, index) => {
              return (
                <li key={index} className="HoverEffect">
                  <a className={item.cName} href={item.url}>
                    {item.title}
                  </a>
                </li>
              );
            })}
          </ul>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

// import React, { Component } from "react";
// import { MenuItems } from "./Menuitem";

// import logo from "../../assets/images/Logo.png";

// import { Link } from "react-router-dom";

// class NavBar extends Component {
//   render() {
//     return (
//       <nav className="NavbarItems">
//         <Link to="/">
//           <img className="navbar-logo " src={logo} alt="CyberMate" />
//         </Link>
//         <ul>
//           {MenuItems.map((item, index) => {
//             return (
//               <li key={index} className="HoverEffect">
//                 <a className={item.cName} href={item.url}>
//                   {item.title}
//                 </a>
//               </li>
//             );
//           })}
//         </ul>
//       </nav>
//     );
//   }
// }
// export default NavBar;
