export const MenuItems = [
  {
    title: "Home",
    url: "/",
    cName: "nav-links",
  },
  {
    title: "About Us",
    url: "/aboutus",
    cName: "nav-links",
  },
  {
    title: "Log in",
    url: "/login",
    cName: "nav-links",
  },
  {
    title: "Sign Up",
    url: "/signup",
    cName: "nav-links",
  },
  //Array of all menu items
];
