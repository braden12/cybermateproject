/*jshint eqeqeq: false */

import * as React from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import StepContent from "@mui/material/StepContent";
import Typography from "@mui/material/Typography";
import "./stepper.css";
import LessonsContext from "../context/lessonsContext";
import { useContext } from "react";
import CustomizedDialogs from "./DialogBox.js";
import { useEffect } from "react";
import { styled } from "@mui/material/styles";
import { ThemeProvider, createTheme } from "@mui/material";

import {
  Lesson0Step0,
  Lesson0Step1,
  Lesson0Step2,
  Lesson0Step3,
  Lesson0Step4,
  Lesson0Step5,
  Lesson0Step6,
  Lesson1Step0,
  Lesson1Step1,
  Lesson1Step2,
  Lesson1Step3,
  Lesson1Step4,
  Lesson1Step5,
  Lesson1Step6,
  Lesson2Step0,
  Lesson2Step1,
  Lesson2Step2,
  Lesson2Step3,
  Lesson2Step4,
  Lesson2Step5,
  Lesson2Step6,
} from "../pages/StepperContent";

const theme = createTheme({
  breakpoints: {
    values: {
      lg: 1903,
      mg: 1440,
      xl: 2730,
    },
  },
});

//Fixed width before was 1200px
const Step_Content = styled("div")(({ theme }) => ({
  [theme.breakpoints.up("mg")]: {
    width: "53.333vw",
    // width: "84.167vw",
  },
  [theme.breakpoints.up("lg")]: {
    width: "63.058vw",
  },
  [theme.breakpoints.up("xl")]: {
    width: "44.314vw",
    // width: "44.314vw",
  },
}));
// Same as content but + 12px's
const STEP_HEADER = styled("div")(({ theme }) => ({
  [theme.breakpoints.up("mg")]: {
    width: "53.333vw",
  },
  [theme.breakpoints.up("lg")]: {
    width: "63.058vw",
  },
  [theme.breakpoints.up("xl")]: {
    width: "44.314vw",
  },
}));

export default function VerticalLinearStepper(props) {
  console.log("Lesson ID: " + props.lessonID);
  console.log("urlParam: " + props.urlParam);
  //extract the data provided by lessonsContext
  const [schema, setSchema, counter, setCounter] = useContext(LessonsContext);

  var schemaCopy = schema; // copy used to update the value of the schema

  var lessonIndex = props.lessonID; // value passed in by parent component to identify the correct lesson

  var lessonIdentifier; // To be used to dereference values from the LessonsContext inside the stepper component

  var arrayOfSteps = []; // Array of steps to be used to create the stepper

  // code to verify the current lesson's index

  schema.lessons.map(
    // iterate through the lessons
    (lesson) =>
      // When the id's match, collect the id
      lessonIndex == lesson.id && (lessonIdentifier = lesson.id)

    // place safeguard here in case lessonIdentifier is never set
  );

  // functions for updating value of counter
  const handleNext = () => {
    setCounter((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setCounter((prevActiveStep) => prevActiveStep - 1);
  };

  const ButtonStyle = () => {
    if (counter < schema.lessons[lessonIdentifier].lessonContent.length - 1) {
      return (
        <div className="NextAndBackButtons">
          <span className="btn btn-primary" onClick={handleNext}>
            Next
          </span>

          {counter !== 0 && (
            <span
              className="btn btn-primary"
              onClick={handleBack}
              // margin bottom 100px
            >
              Back
            </span>
          )}
        </div>
      );
    } else {
      return <CustomizedDialogs func={handleBack} id={lessonIdentifier} />;
    }
  };

  // initialise counter as value from the current lesson
  React.useEffect(() => {
    setCounter(schema.lessons[lessonIndex].currentIndex);
  }, [props.urlParam]); // runs on every refresh or lessonChange

  // scroll smoothly to the top of the page when counter changes with increments as the counter changes
  useEffect(() => {
    // if the counter is 0, scroll to the top of the page
    if (counter === 0) {
      // console.log("if runs  ");
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
    // else scroll to the top of the page with an offset of 100px
    else {
      // console.log("else runs");
      window.scrollTo({
        top: counter * 44 + 130,
        left: 0,
        behavior: "smooth",
      });
    }
  }, [counter]);

  // updates currentIndex in the context to the activestep whenever the active step changes the step of the stepper
  React.useEffect(() => {
    localStorage.setItem("counter", JSON.stringify(counter)); // places and updates schema in localstorage

    // console.log(JSON.parse(localStorage.getItem("counter"))); // verify they are in fact in storage
  }, [counter]);

  React.useEffect(() => {
    schemaCopy.lessons[lessonIndex].currentIndex = counter;

    setSchema(schemaCopy); // set the schema to the altered schema value (recommended implementation)

    localStorage.setItem("schema", JSON.stringify(schema)); // places and updates schema in localstorage
  }, [counter]);

  return (
    <ThemeProvider theme={theme}>
      <Box
        className="Box-component"
        /*sx={{ backgroundColor: "white"}}*/
      >
        <Stepper
          activeStep={counter}
          orientation="vertical"
          style={{ color: "#ba2c73" }}
        >
          {schema.lessons[lessonIdentifier].lessonContent.map(
            (content, index) => (
              <Step
                key={content.contentHeading}
                sx={{
                  "& .MuiStepLabel-root .Mui-completed": {
                    color: "#ba2c73", // circle color (COMPLETED)
                  },
                  "& .MuiStepLabel-root .Mui-active": {
                    color: "#ba2c73", // circle color (ACTIVE)
                  },
                  "& .MuiStepLabel-root .Mui-active .MuiStepIcon-text": {
                    fill: "white", // circle's number (ACTIVE)
                  },
                }}
              >
                <STEP_HEADER onClick={() => setCounter(index)}>
                  <div>
                    <StepLabel
                      sx={{
                        padding: 0,
                      }}
                    >
                      <Typography
                        component={"div"}
                        // sx={{ maxWidth: 1200 }}
                        className="Typography"
                        style={{
                          fontWeight: 700,
                          maxWidth: "900px",
                          fontSize: "1.5rem",
                          cursor: "pointer",
                        }}
                      >
                        {content.contentHeading}
                      </Typography>
                    </StepLabel>
                  </div>
                </STEP_HEADER>
                <Step_Content>
                  <StepContent
                    sx={{
                      padding: "0px 0px 0px 19px",
                      margin: "0px 0px 0px 12px",
                    }}
                  >
                    <Typography
                      component={"div"}
                      className="StepContent"
                      sx={{ fontSize: "1.2rem" }}
                    >
                      {/* Conditionally render the correct step depending on the index and the lessonIdentifer */}
                      {index === 0 && lessonIdentifier == 0 && <Lesson0Step0 />}
                      {index === 1 && lessonIdentifier == 0 && <Lesson0Step1 />}
                      {index === 2 && lessonIdentifier == 0 && <Lesson0Step2 />}
                      {index === 3 && lessonIdentifier == 0 && <Lesson0Step3 />}
                      {index === 4 && lessonIdentifier == 0 && <Lesson0Step4 />}
                      {index === 5 && lessonIdentifier == 0 && <Lesson0Step5 />}
                      {index === 6 && lessonIdentifier == 0 && <Lesson0Step6 />}
                      {index === 0 && lessonIdentifier == 1 && <Lesson1Step0 />}
                      {index === 1 && lessonIdentifier == 1 && <Lesson1Step1 />}
                      {index === 2 && lessonIdentifier == 1 && <Lesson1Step2 />}
                      {index === 3 && lessonIdentifier == 1 && <Lesson1Step3 />}
                      {index === 4 && lessonIdentifier == 1 && <Lesson1Step4 />}
                      {index === 5 && lessonIdentifier == 1 && <Lesson1Step5 />}
                      {index === 6 && lessonIdentifier == 1 && <Lesson1Step6 />}
                      {index === 0 && lessonIdentifier == 2 && <Lesson2Step0 />}
                      {index === 1 && lessonIdentifier == 2 && <Lesson2Step1 />}
                      {index === 2 && lessonIdentifier == 2 && <Lesson2Step2 />}
                      {index === 3 && lessonIdentifier == 2 && <Lesson2Step3 />}
                      {index === 4 && lessonIdentifier == 2 && <Lesson2Step4 />}
                      {index === 5 && lessonIdentifier == 2 && <Lesson2Step5 />}
                      {index === 6 && lessonIdentifier == 2 && <Lesson2Step6 />}
                    </Typography>

                    <Box
                      sx={{
                        height: "120px",
                        paddingTop: "-20px",
                      }}
                    >
                      <ButtonStyle />
                    </Box>
                  </StepContent>
                </Step_Content>
              </Step>
            )
          )}
        </Stepper>
      </Box>
    </ThemeProvider>
  );
}
