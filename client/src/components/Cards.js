import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import LinearProgress from "@mui/material/LinearProgress";
import { CardActionArea } from "@mui/material";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";

// Colour pallete required for the progress bar
const theme = createTheme({
  palette: {
    primary: {
      main: "#0e7c86",
    },
  },
});

const MediaCard = ({ Image, AltImgtxt, heading, body, LsnLink, progress }) => {
  return (
    <CardActionArea
      component={Link}
      to={LsnLink}
      sx={{
        maxWidth: 300,
        minHeight: 360,
        "&:hover": {
          boxShadow:
            "0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)",
          transition: "all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1)",
        },
      }}
      // Give hover animation a shadow effect
    >
      <Card sx={{ maxWidth: 300, minHeight: 360, overflow: "hidden" }}>
        <CardMedia
          component="img" //Used to determine what is being rendered in the card
          height="200px"
          width="300"
          image={Image}
          alt={AltImgtxt}
        />
        <CardContent sx={{ overflow: "hidden" }}>
          <ThemeProvider theme={theme}>
            <LinearProgress
              variant="determinate"
              value={progress}
              style={{ margin: -16 }}
              color="primary"
              sx={{ height: 7, borderRadius: 0 }}
            />
          </ThemeProvider>
        </CardContent>
        <CardContent>
          <Typography
            component={"div"}
            gutterBottom
            variant="h5"
            style={{ marginTop: -30 }}
          >
            {heading}
          </Typography>
          <Typography component={"div"} variant="body2" color="text.secondary">
            {body}
          </Typography>
        </CardContent>
      </Card>
    </CardActionArea>
  );
};

export default MediaCard;
