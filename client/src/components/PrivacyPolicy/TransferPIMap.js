import React, { Component } from "react";
import { TransferPIItem } from "./TransferPIItem";

class TransferPIMap extends Component {
  render() {
    return (
      <ul>
        {TransferPIItem.map((text, index) => {
          return (
            <li key={index}>
              <h5>{text.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default TransferPIMap;
