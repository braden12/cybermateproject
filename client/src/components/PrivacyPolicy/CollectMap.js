import React, { Component } from "react";
import { CollectItems } from "./CollectItems";

class CollectMap extends Component {
  render() {
    return (
      <ul>
        {CollectItems.map((text, index) => {
          return (
            <li key={index}>
              <h5>{text.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default CollectMap;
