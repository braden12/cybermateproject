export const UseInfoItems = [
    {
      name: "(I) Deliver to you a more personalised experience and service offering",
    },
    {
      name: '(II) Improve the quality of the services we offer',
    },
    {
      name: '(III) Internal administrative purposes',
    },
    {
      name: '(IV) Marketing and research purposes',
    },
];