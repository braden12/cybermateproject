export const TransferPIItem = [
    {
      name: '(I) You have given us your consent to disclose personal information to that third party',
    },
    {
      name: '(II) We reasonably believe that the overseas recipient is subject to a law or binding scheme that is, overall, substantially similar to the APPs and the law or binding scheme can be enforced; or the disclosure is required or authorised by an Australian law or court / tribunal order',
    },
];