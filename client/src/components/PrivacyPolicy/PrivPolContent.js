import {Component} from "react";
import CollectMap from "./CollectMap";
import UseInfoMap from "./UseInfoMap";
import DiscloseMap from "./DiscloseMap";
import TransferPIMap from "./TransferPIMap";
import ProtectInfoMap from "./ProtectInfoMap";

class PrivPolContent extends Component{
    render(){
        return (
            <div>
                <h2>Privacy Policy</h2>
                                <h4>Last updated: August 4th, 2022</h4>
                                <h3>1. Cycademy's stance on privacy</h3>
                                <h5>
                                    Cycademy values and respects the privacy of the people we deal with. Cycademy is committed to
                                    protecting your privacy and complying with the Privacy Act 1988 (Cth) (Privacy Act) and other applicable
                                    privacy laws and regulations.
                                    This Privacy Policy (Policy) describes how we collect, hold, use and disclose your personal information, and
                                    how we maintain the quality and security of your personal information.
                                </h5>
                                <h3>2. What is personal information?</h3>
                                <h5>
                                    “Personal information” means any information or opinion, whether true or not, and whether recorded in a
                                    material form or not, about an identified individual or an individual who is reasonably identifiable. In general
                                    terms, this includes information or an opinion that personally identifies you either directly (e.g. your name) or
                                    indirectly
                                </h5>
                                <h3>3. What personal information do we collect?</h3>
                                <h5>
                                    The personal information we collect about you depends on the nature of your dealings with us or what you choose to share with us. The personal information we collect about you may include:
                                    <CollectMap/>
                                </h5>
                                <h5>
                                    [Under certain circumstances, Cycademy may need to collect sensitive information about you. This might
                                    include any information or opinion about your racial or ethnic origin, political opinions, political association,
                                    religious or philosophical beliefs, membership of a trade union or other professional body, sexual preferences,
                                    criminal record, or health information.
                                </h5>
                                <h5>
                                    If we collect your sensitive information, we will do so only with your consent, if it is necessary to prevent a
                                    serious and imminent threat to life or health, or as otherwise required or authorised by law, and we take
                                    appropriate measures to protect the security of this information.]
                                </h5>
                                <h5>
                                    You do not have to provide us with your personal information. Where possible, we will give you the option to
                                    interact with us anonymously or by using a pseudonym. However, if you choose to deal with us in this way or
                                    choose not to provide us with your personal information, we may not be able to provide you with our services or
                                    otherwise interact with you.
                                </h5>
                                <h3>4. How do we collect your personal information?</h3>
                                <h5>
                                    We collect your personal information directly from you when you create an account by signing up for our services.
                                </h5>
                                <h3>5. Collecting personal information from third parties</h3>
                                <h5>
                                    We may also collect your personal information from third parties or through publicly available sources, for
                                    example from [insert third parties who your organisation collects personal information from]. We collect your
                                    personal information from these third parties so that [insert the purpose for which your organisation collects
                                    personal information from these third parties].
                                </h5>
                                <h3>6. How do we use your personal information?</h3>
                                <h5>
                                    We use personal information for many purposes in connection with our functions and activities, including the following purposes:
                                    <UseInfoMap/>
                                </h5>
                                <h3>7. Disclosure of personal information to third parties</h3>
                                <h5>
                                    We may disclose your personal information to third parties in accordance with this Policy in circumstances
                                    where you would reasonably expect us to disclose your information. For example, we may disclose your
                                    personal information to:
                                    <DiscloseMap/>
                                </h5>
                                <h3>8. Transfer of personal information overseas</h3>
                                <h5>
                                    Some of the third-party service providers we disclose personal information to may be based in or have servers
                                    located outside of Australia, including in [insert overseas countries where third parties are located / have
                                    servers].
                                    Where we disclose your personal information to third parties overseas, we will take reasonable steps to ensure
                                    that data security and appropriate privacy practices are maintained. We will only disclose to overseas third
                                    parties if
                                    <TransferPIMap/>
                                </h5>
                                <h3>9. How do we protect your personal information?</h3>
                                <h5>
                                    Cycademy will take reasonable steps to ensure that the personal information that we hold about you is kept
                                    confidential and secure, including by:
                                    <ProtectInfoMap/>
                                </h5>
                </div>
        );
    }
}
export default PrivPolContent;