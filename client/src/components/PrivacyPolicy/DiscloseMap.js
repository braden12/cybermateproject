import React, { Component } from "react";
import {DiscloseItem} from "./DiscloseItem";

class DiscloseMap extends Component {
  render() {
    return (
      <ul>
        {DiscloseItem.map((text, index) => {
          return (
            <li key={index}>
              <h5>{text.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default DiscloseMap;
