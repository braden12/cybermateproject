import React, { Component } from "react";
import { ProtectInfoItem } from "./ProtectInfoItem";

class ProtectInfoMap extends Component {
  render() {
    return (
      <ul>
        {ProtectInfoItem.map((text, index) => {
          return (
            <li key={index}>
              <h5>{text.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default ProtectInfoMap;
