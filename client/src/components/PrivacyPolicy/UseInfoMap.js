import React, { Component } from "react";
import { UseInfoItems } from "./UseInfoItem";

class UseInfoMap extends Component {
  render() {
    return (
      <ul>
        {UseInfoItems.map((text, index) => {
          return (
            <li key={index}>
              <h5>{text.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default UseInfoMap;
