export const ProtectInfoItem = [
    {
      name: '(I) Ensuring that all stored passwords are hashed.',
    },
    {
      name: '(II) Users are authenticated and only have access to data they are authenticated for if they have the URI token.',
    },
    {
      name: '(III) Having a robust physical security of our premises and databases / records',
    },
    {
      name: '(IV) Taking measures to restrict access to only personnel who need that personal information to effectively provide services to you',
    },
    {
      name: '(V) Having technological measures in place (for example, anti-virus software, fire walls)',
    },

];