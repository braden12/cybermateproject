export const DiscloseItem = [
    {
      name: '(I) our third party service providers (for example, our IT providers)',
    },
    {
      name: '(II) our marketing providers;',
    },
    {
      name: '(III) our professional services advisors',
    },
];