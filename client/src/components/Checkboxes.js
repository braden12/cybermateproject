import * as React from "react";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import LessonAlert from "./lessonAlert";
import { useContext } from "react";
import LessonsContext from "../context/lessonsContext";

// Checkboxes Quiz Component
const Checkboxes = (props) => {
  // console.log("answerList: ", props.answerList);
  const answerList = props.answerList;
  // console.log("question: ", props.question);
  // console.log("question2: ", props.question);
  const question = props.question;
  // console.log("question: ", question);
  const incorrectAnswerText = props.incorrectAnswerText;
  // console.log("incorrectAnswerText: ", incorrectAnswerText);
  const correctAnswerText = props.correctAnswerText;
  // console.log("correctAnswerText: ", correctAnswerText);
  // console.log("lessonNumber: ", props.lessonNumber);
  // console.log("quizNumber: ", props.quizNumber);

  // get context schema for lesson
  const [schema, setSchema] = useContext(LessonsContext);

  // get lesson quiz values
  const schema2 = schema;

  var checkedAnswers = [];

  // Check if the context schema quizAnswers are empty or undefined
  if (typeof schema2.lessons[props.lessonNumber].quizAnswers === "undefined") {
    // if empty or undefined, set the quizAnswers to an empty array of arrays of length quizzesDone from the context
    schema2.lessons[props.lessonNumber].quizAnswers = Array(
      schema2.lessons[props.lessonNumber].quizzesDone.length
    ).fill([]);
    setSchema(schema2);
  }

  // check if quizAnswers[quizNumber] is not empty or undefined
  if (
    typeof schema2.lessons[props.lessonNumber].quizAnswers[props.quizNumber] !==
    "undefined"
  ) {
    // if there is a value inside, save the value to be used for the state of isChecked
    checkedAnswers =
      schema2.lessons[props.lessonNumber].quizAnswers[props.quizNumber];
  } else {
    // if there isn't a value inside, fill it with an array of length answerList.length of false
    schema2.lessons[props.lessonNumber].quizAnswers[props.quizNumber] = Array(
      answerList.length
    ).fill(false);
    setSchema(schema2);
    // save the value to be used for the state of isChecked
    checkedAnswers =
      schema2.lessons[props.lessonNumber].quizAnswers[props.quizNumber];
  }

  // console.log(checkedAnswers);

  // CODE ABOVE FOR HANDLING STATE

  // isChecked controls the state of the checkboxes
  const [isChecked, setIsChecked] = React.useState(checkedAnswers);

  // whenever isChecked is updated, update the context schema
  React.useEffect(() => {
    schema2.lessons[props.lessonNumber].quizAnswers[props.quizNumber] =
      isChecked;
    setSchema(schema2);
    localStorage.setItem("schema", JSON.stringify(schema)); // places and updates schema in localstorage
  }, [isChecked]);

  // console.log("isChecked: ", isChecked);
  // console.log(schema.lessons[props.lessonNumber].quizAnswers);

  // returns the values of the isChecked array
  const isCheckboxChecked = (index, checked) => {
    setIsChecked((isChecked) => {
      return isChecked.map((c, i) => {
        if (i === index) return checked;
        return c;
      });
    });
  };

  // initialise values from context schema or create them if they don't exist

  // check if quizAppearanceVariables in the context already exists
  if (
    typeof schema.lessons[props.lessonNumber].quizAppearanceVariables ===
    "undefined"
  ) {
    // if it doesn't create an array of empty arrays of length quizzesDone
    schema.lessons[props.lessonNumber].quizAppearanceVariables = Array(
      schema.lessons[props.lessonNumber].quizzesDone.length
    ).fill([]);
    setSchema(schema);
  }

  // check if the quizAppearanceVariables[quizNumber] is empty or undefined
  if (
    typeof schema.lessons[props.lessonNumber].quizAppearanceVariables[
      props.quizNumber
    ] === "undefined"
  ) {
    // if it is, fill it with an array of length 4 of false
    schema.lessons[props.lessonNumber].quizAppearanceVariables[
      props.quizNumber
    ] = Array(4).fill(false);
    // grab it to use for the state variables below
    var grab =
      schema.lessons[props.lessonNumber].quizAppearanceVariables[
        props.quizNumber
      ];
  } else {
    // if it isn't, grab it to use for the state variables below
    var grab =
      schema.lessons[props.lessonNumber].quizAppearanceVariables[
        props.quizNumber
      ];
  }

  // state to control whether checkboxes are clickable
  const [disabled, setDisabled] = React.useState(grab[0]);

  // state to control whether the reset button is shown
  const [showReset, setShowReset] = React.useState(grab[1]);

  // Define isAlertVisible state, used for each quiz element to determine if alert is visible
  const [alertVisible, setAlertVisible] = React.useState(grab[2]);

  // Used to determine if success for danger alert is displayed
  const [lessonSuccess, setLessonSuccess] = React.useState(grab[3]);

  // When any of the above 4 values change, update the context schema
  React.useEffect(() => {
    schema.lessons[props.lessonNumber].quizAppearanceVariables[
      props.quizNumber
    ] = [disabled, showReset, alertVisible, lessonSuccess];
    setSchema(schema);
    localStorage.setItem("schema", JSON.stringify(schema)); // places and updates schema in localstorage
  }, [disabled, showReset, alertVisible, lessonSuccess]);

  // LOGIC FOR barring user from finishing a lesson until all quizzes are complete

  // check if all correct answers are checked and none of the wrong answers are checked
  const allAnswersCorrect = () => {
    // filter for correct answers
    const correctAnswers = answerList.filter((i) => i.correctAnswer === true);
    const correctAnswersChecked = correctAnswers.filter(
      (i) => isChecked[i.Num] === true
    );
    // filter for wrong answers
    const wrongAnswers = answerList.filter((i) => i.correctAnswer === false);
    const wrongAnswersChecked = wrongAnswers.filter(
      (i) => isChecked[i.Num] === true
    );

    // if all correct answers are checked and none of the wrong answers are checked, return true
    if (
      correctAnswers.length === correctAnswersChecked.length &&
      wrongAnswersChecked.length === 0
    ) {
      setAlertVisible(true);
      return true;
    }
    // if not, return false
    setAlertVisible(false);
    return false;
  };

  // submit quiz answers, handle success and failure
  const onSubmit = (e) => {
    e.preventDefault();

    if (allAnswersCorrect()) {
      // if disabled already true, don't do anything
      if (disabled) {
        console.log("disabled already true");
        return;
      } else {
        // else set disabled to true
        console.log("TOGGLING DISABLED");
        toggleDisabled();
        setLessonSuccess(true);
        setAlertVisible(true);
        // update the schema to show that this quiz is done
        schema2.lessons[props.lessonNumber].quizzesDone[
          props.quizNumber
        ] = true;
        setSchema(schema2);
        console.log("schema: ", schema);
      }
    } else {
      // if disabled already true, don't do anything
      if (disabled) {
        console.log("disabled already true 2");
        return;
      } else {
        // else set disabled to true
        console.log("TOGGLING DISABLED 2");
        toggleDisabled();
        setLessonSuccess(false);
        setShowReset(true);
        setAlertVisible(true);
        // update the schema to show that this quiz is not Done
        schema2.lessons[props.lessonNumber].quizzesDone[
          props.quizNumber
        ] = false;
        setSchema(schema2);
      }
    }
    console.log("END OF ONSUBMIT");
  };

  const toggleDisabled = () => {
    setDisabled((disabled) => !disabled);
  };

  const resetCheckboxes = () => {
    setIsChecked(() => answerList.map((i) => false));
    setDisabled(false);
    setShowReset(false);
    setAlertVisible(false);
  };

  return (
    <div>
      <form onSubmit={onSubmit}>
        <h5>{question}</h5>

        {answerList.map((checkbox, index) => {
          return (
            <FormGroup key={index + checkbox.name}>
              {/* FormGroup vertically aligns checkboxes */}

              {/* MUI component for checkbox component */}
              <FormControlLabel
                control={
                  <Checkbox
                    disabled={disabled}
                    name={checkbox.name}
                    value={checkbox.weight}
                    id={checkbox.category}
                    checked={isChecked[index]}
                    color="primary"
                    onChange={(e) => isCheckboxChecked(index, e.target.checked)}
                    sx={{
                      color: "#BA2C73",
                      "&.Mui-checked": { color: "#BA2C73" },
                      // set disabled colour dark gray
                      "&.Mui-disabled": { color: "#666666" },
                    }}
                  />
                }
                label={checkbox.answer}
              />
            </FormGroup>
          );
        })}

        {/* {showAlert && <Alert />} */}
        {alertVisible && (
          <LessonAlert
            success={lessonSuccess}
            incorrectAnswerText={incorrectAnswerText}
            correctAnswerText={correctAnswerText}
          />
        )}

        <span
          className="btn btn-primary"
          onClick={onSubmit}
          style={{ marginBottom: "20px" }}
        >
          Check Answer
        </span>

        {/* Button that resets checkboxes */}
        {showReset && (
          <span
            className="btn btn-primary"
            onClick={() => resetCheckboxes()}
            style={{ marginBottom: "20px" }}
          >
            Retry
          </span>
        )}
      </form>
    </div>
  );
};

export default Checkboxes;
