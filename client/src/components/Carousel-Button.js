import Button from "@mui/material/Button";
import { consts } from "react-elastic-carousel";

const CarouselButton = ({ type, onClick }) => {
  const pointer = type === consts.PREV ? "<" : ">";
  return (
    <Button
      sx={{
        borderRadius: "50%",
        backgroundColor: "white",
        height: 65,
        marginTop: 15,
        width: 60,
        "&:hover": {
          backgroundColor: "white",
          boxShadow:
            "0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)",
          transition: "all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1)",
        },
        // give onclick flash animation
        "&:active": {
          backgroundColor: "lightGray",
          transition: "all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1)",
        },
        fontSize: "xx-large",
        color: "#BA2C73",
      }}
      onClick={onClick}
      disableRipple
    >
      {pointer}
    </Button>
  );
};

export default CarouselButton;
