import React, { Component } from "react";
import { TermsItems } from "./TermsItems";

class TermsList extends Component {
  render() {
    return (
      <ul>
        {TermsItems.map((Warranty, index) => {
          return (
            <li key={index}>
              <h5>{Warranty.name}</h5>
            </li>
          );
        })}
      </ul>
    );
  }
}
export default TermsList;
