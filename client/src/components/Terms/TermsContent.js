import TermsList from "./TermsList";
import { Link } from "react-router-dom";
import { Component } from "react";

class TermsContent extends Component {
  render() {
    return (
      <div>
        <h2>Terms and conditions</h2>
        <h4>
          Welcome to WWW.Cycademy.com This site is provided as a service to our
          visitors and may be used for informational purposes only. Because the
          Terms and Conditions contain legal obligations, please read them
          carefully.
        </h4>
        <h4>Last updated: August 4th, 2022</h4>
        <h3>1. YOUR AGREEMENT</h3>
        <h5>
          By using this Site, you agree to be bound by, and to comply with,
          these Terms and Conditions. If you do not agree to these Terms and
          Conditions, please do not use this site. PLEASE NOTE: We reserve the
          right, at our sole discretion, to change, modify or otherwise alter
          these Terms and Conditions at any time. Unless otherwise indicated,
          amendments will become effective immediately. Please review these
          Terms and Conditions periodically. Your continued use of the Site
          following the posting of changes and/or modifications will constitute
          your acceptance of the revised Terms and Conditions and the
          reasonableness of these standards for notice of changes. For your
          information, this page was last updated as of the date at the top of
          these terms and conditions.
        </h5>
        <h3>2. PRIVACY</h3>
        <h5>
          Please review our{" "}
          <Link to="/privacypolicy" className="member-btn">
            privacy policy
          </Link>
          , which also governs your visit to this Site, to understand our
          practices.
        </h5>
        <h3>3. LINKED SITES</h3>
        <h5>
          This Site may contain links to other independent third-party Web sites
          ("Linked Sites”). These Linked Sites are provided solely as a
          convenience to our visitors. Such Linked Sites are not under our
          control, and we are not responsible for and does not endorse the
          content of such Linked Sites, including any information or materials
          contained on such Linked Sites. You will need to make your own
          independent judgment regarding your interaction with these Linked
          Sites.
        </h5>
        <h3>4. FORWARD LOOKING STATEMENTS</h3>
        <h5>
          All materials reproduced on this site speak as of the original date of
          publication or filing. The fact that a document is available on this
          site does not mean that the information contained in such document has
          not been modified or superseded by events or by a subsequent document
          or filing. We have no duty or policy to update any information or
          statements contained on this site and, therefore, such information or
          statements should not be relied upon as being current as of the date
          you access this site.
        </h5>
        <h3>5. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY</h3>
        <h5>
          A. THIS SITE MAY CONTAIN INACCURACIES AND TYPOGRAPHICAL ERRORS. WE
          DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE MATERIALS OR THE
          RELIABILITY OF ANY ADVICE, OPINION, STATEMENT OR OTHER INFORMATION
          DISPLAYED OR DISTRIBUTED THROUGH THE SITE. YOU EXPRESSLY UNDERSTAND
          AND AGREE THAT:
          <TermsList />
          B. YOU UNDERSTAND AND AGREE THAT UNDER NO CIRCUMSTANCES, INCLUDING,
          BUT NOT LIMITED TO, NEGLIGENCE, SHALL WE BE LIABLE FOR ANY DIRECT,
          INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES THAT
          RESULT FROM THE USE OF, OR THE INABILITY TO USE, ANY OF OUR SITES OR
          MATERIALS OR FUNCTIONS ON ANY SUCH SITE, EVEN IF WE HAVE BEEN ADVISED
          OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATIONS SHALL
          APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED
          REMEDY.
        </h5>
        <h3>6. OUR PROPRIETARY RIGHTS</h3>
        <h5>
          This Site and all its Contents are intended solely for personal,
          non-commercial use. Except as expressly provided, nothing within the
          Site shall be construed as conferring any license under our or any
          third party's intellectual property rights, whether by estoppel,
          implication, waiver, or otherwise. Without limiting the generality of
          the foregoing, you acknowledge and agree that all content available
          through and used to operate the Site and its services is protected by
          copyright, trademark, patent, or other proprietary rights. You agree
          not to: (a) modify, alter, or deface any of the trademarks, service
          marks, trade dress (collectively "Trademarks") or other intellectual
          property made available by us in connection with the Site; (b) hold
          yourself out as in any way sponsored by, affiliated with, or endorsed
          by us, or any of our affiliates or service providers; (c) use any of
          the Trademarks or other content accessible through the Site for any
          purpose other than the purpose for which we have made it available to
          you; (d) defame or disparage us, our Trademarks, or any aspect of the
          Site; and (e) adapt, translate, modify, decompile, disassemble, or
          reverse engineer the Site or any software or programs used in
          connection with it or its products and services. The framing,
          mirroring, scraping or data mining of the Site or any of its content
          in any form and by any method is expressly prohibited.
        </h5>
        <h3>7. INDEMNITY</h3>
        <h5>
          By using the Site web sites you agree to indemnify us and affiliated
          entities (collectively "Indemnities") and hold them harmless from any
          and all claims and expenses, including (without limitation) attorney's
          fees, arising from your use of the Site web sites, your use of the
          Products and Services, or your submission of ideas and/or related
          materials to us or from any person's use of any ID, membership or
          password you maintain with any portion of the Site, regardless of
          whether such use is authorized by you.
        </h5>
        <h3>8. COPYRIGHT AND TRADEMARK NOTICE</h3>
        <h5>
          Except our generated free lessons, which is free to use for private
          and commercial use, all other content is copyrighted. Cycademy © 2022,
          all rights reserved
        </h5>
        <h3>9. INTELLECTUAL PROPERTY INFRINGEMENT CLAIMS</h3>
        <h5>
          It is our policy to respond expeditiously to claims of intellectual
          property infringement. We will promptly process and investigate
          notices of alleged infringement and will take appropriate actions
          under the Digital Millennium Copyright Act ("DMCA") and other
          applicable intellectual property laws. Notices of claimed infringement
          should be directed to:
        </h5>
        <h5>Cycademy.com</h5>
        <h5>25 Northfields Ave.</h5>
        <h5>Wollongong,NSW,2522 Australia</h5>
        <h5>cycademybusiness@gmail.com</h5>
      </div>
    );
  }
}
export default TermsContent;
