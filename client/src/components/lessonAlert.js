const LessonAlert = (props) => {
  // console.log("THIS IS THE LESSON ALERT FUNCTION");
  // console.log("props.success: ", props.success);

  // check if alertType is danger or success
  if (props.success === undefined) {
    return null;
  } else if (props.success === true) {
    return (
      <div className="alert alert-success" role="alert">
        {props.correctAnswerText}
      </div>
    );
  } else if (props.success === false) {
    return (
      <div className="alert alert-danger" role="alert">
        {props.incorrectAnswerText}
      </div>
    );
  }
};

export default LessonAlert;
