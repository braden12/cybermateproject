import { Component } from "react";
import "./Homebody.css";
import { Link } from "react-router-dom";

class Homebody extends Component {
  render() {
    return (
      <div className="MainScreen">
        <div className="Box">
          <h3>Cybersecurity Awareness Training & Resources</h3>
          <div className="BodyContent">
            <b>
              In 2021 the Australian Cyber Security Centre (ACSC) reported that:
            </b>

            <ul>
              <li>
                It had 1 report of a cyberattack every
                <a
                  href="https://www.cyber.gov.au/acsc/view-all-content/reports-and-statistics/acsc-annual-cyber-threat-report-july-2020-june-2021"
                  target="_blank"
                  rel="noreferrer"
                >
                  <b> 8 minutes</b>
                </a>
              </li>
              <li>
                Self-reported losses from cybercrime total
                <a
                  href="https://www.cyber.gov.au/sites/default/files/2021-09/2020-21-ar-the-acsc-observed.pdf"
                  target="_blank"
                  rel="noreferrer"
                >
                  <b> more than $33 billion.</b>
                </a>
              </li>
              <li>
                75% of Australians reporting were
                <a
                  href="https://www.cyber.gov.au/sites/default/files/2021-09/2020-21-ar-the-acsc-observed.pdf"
                  target="_blank"
                  rel="noreferrer"
                >
                  <b> losing money or personal information.</b>
                </a>
              </li>
            </ul>

            <Link to="/signup" className="btn btn-hero">
              Join Now
            </Link>

            <h4 className="Why">Why Cycademy?</h4>

            <table>
              <thead>
                <tr>
                  <th>
                    <a
                      href="https://www.flaticon.com/free-icons/free"
                      title="free icons"
                    >
                      <img
                        src="/static/Icons/HomepageIcons/free.png"
                        alt="Free"
                        className="Icon"
                      />
                    </a>
                  </th>
                  <th>
                    <a
                      href="https://www.flaticon.com/free-icons/target"
                      title="target icons"
                    >
                      <img
                        src="/static/Icons/HomepageIcons/target.png"
                        alt="Book"
                        className="Icon"
                      />
                    </a>
                  </th>
                  <th>
                    <a
                      href="https://www.flaticon.com/free-icons/productivity"
                      title="productivity icons"
                    >
                      <img
                        src="/static/Icons/HomepageIcons/clock.png"
                        alt="Clock"
                        className="Icon"
                      />
                    </a>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Free</th>
                  <th>Relevant</th>
                  <th>Efficient</th>
                </tr>
                <tr>
                  <td> No related expenses</td>
                  <td>
                    Lessons are up to date with the most relevant threats and
                    trends
                  </td>
                  <td>Short lessons designed to fit into your schedule</td>
                </tr>
              </tbody>
            </table>

            <br />
            <p className="verticalList">
              <b>Cycademy</b> is a cybersecurity training platform designed to
              educate individuals and Small Businesses on:
            </p>
            <ul className="verticalList">
              <li>What threats are out there</li>
              <li>How to defend against attacks</li>
              <li>What to do after an attack</li>
            </ul>
            <div className="lastTable">
              <table>
                <thead>
                  <tr>
                    <th>
                      <a
                        href="https://www.flaticon.com/free-icons/education"
                        title="education icons"
                      >
                        <img
                          src="/static/Icons/HomepageIcons/online-learning.png"
                          alt="Free"
                          className="Icon"
                          title="Free icons created by Freepik - Flaticon"
                        />
                      </a>
                    </th>
                    <th>
                      <a
                        href="https://www.flaticon.com/free-icons/information"
                        title="information icons"
                      >
                        <img
                          src="/static/Icons/HomepageIcons/search.png"
                          alt="Book"
                          className="Icon"
                        />
                      </a>
                    </th>
                    <th>
                      <a
                        href="https://www.flaticon.com/free-icons/process"
                        title="process icons"
                      >
                        <img
                          src="/static/Icons/HomepageIcons/management.png"
                          alt="Clock"
                          className="Icon"
                        />
                      </a>
                    </th>
                    <th>
                      <a
                        href="https://www.flaticon.com/free-icons/work-in-progress"
                        title="work-in-progress icons"
                      >
                        <img
                          src="/static/Icons/HomepageIcons/work-in-progress.png"
                          alt="Clock"
                          className="Icon"
                        />
                      </a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Learn</th> <th>Test</th> <th>Practice</th>
                    <th>Track</th>
                  </tr>
                  <tr>
                    <td>About cybersecurity in a fun and engaging way.</td>

                    <td>Your knowledge with quizzes.</td>
                    <td>Your skills with interactive exercises.</td>
                    <td>Your progress with a dashboard.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Homebody;
