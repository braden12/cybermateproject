export const Footeritem = [
  {
    title: "About Us",
    url: "/aboutus",
    cName: "foot-links",
  },
  {
    title: "Contact Us",
    url: "/contact",
    cName: "foot-links",
  },
  {
    title: "FAQs",
    url: "/FAQ",
    cName: "foot-links",
  },
];
