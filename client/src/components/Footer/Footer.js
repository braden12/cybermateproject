import React, { Component } from "react";
import { Footeritem } from "./Footeritem";
import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <footer className="FooterItems">
        <ul>
          {Footeritem.map((item, index) => {
            return (
              <li key={index} className="HoverEffect">
                <a className={item.cName} href={item.url}>
                  {item.title}
                </a>
              </li>
            );
          })}
        </ul>
        <div className="gap"></div>
        <a className="follow">Follow Us</a>
        <a
          href="https://www.youtube.com/channel/UCd8NqRjQ7MXjuiHb6wz9d2A"
          target="_blank"
        >
          <i className="fa-brands fa-youtube"></i>
        </a>
        <a href="https://www.instagram.com/cycademy_capstone/" target="_blank">
          <i className="fa-brands fa-instagram"></i>
        </a>
        <a href="https://twitter.com/Cycademy2" target="_blank">
          <i className="fa-brands fa-twitter"></i>
        </a>
        <a
          href="https://www.facebook.com/profile.php?id=100086387547025"
          target="_blank"
        >
          <i className="fa-brands fa-facebook"></i>
        </a>
      </footer>
    );
  }
}
export default Footer;
