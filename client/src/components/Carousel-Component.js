import React, { useState } from "react";
import Carousel, { consts } from "react-elastic-carousel";
import styles from "./styles.css";
import MediaCard from "./Cards";

import LessonsContext from "../context/lessonsContext";
import { useContext } from "react";
import CarouselButton from "./Carousel-Button";

const breakPoints = [
  { width: 350, itemsToShow: 1 },
  { width: 500, itemsToShow: 2 },
  { width: 1000, itemsToShow: 3 },
  { width: 1500, itemsToShow: 4 },
  //Show x amount of cards based on the width of the browser
];

const Carousel_Component = () => {
  const [schema] = useContext(LessonsContext); //extract the data provided by lessonsContext

  return (
    <div style={{ minHeight: "400px" }}>
      <Carousel renderArrow={CarouselButton} breakPoints={breakPoints}>
        {schema.lessons.map(
          // for each lesson
          (
            lesson // represents a lesson, 1 card per lesson
          ) =>
            // where the lesson module is threat-landscape
            lesson.module === "threat-landscape" && (
              <MediaCard
                key={lesson.id}
                Image={lesson.image}
                AltImgtxt={lesson.altImgtxt}
                heading={lesson.title}
                body={lesson.description}
                LsnLink={
                  lesson.urlParam !== ""
                    ? "/lessons/" + lesson.module + "/" + lesson.urlParam
                    : ""
                }
                progress={
                  lesson.isDone
                    ? 100
                    : (lesson.currentIndex / lesson.lessonContent.length) * 100
                }
              ></MediaCard>
            )
        )}
      </Carousel>
    </div>
  );
};

export default Carousel_Component;
