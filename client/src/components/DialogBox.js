import * as React from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import HomeIcon from "@mui/icons-material/Home";
import "./DialogBox.css";
import { Link } from "react-router-dom";
import LessonsContext from "../context/lessonsContext";
import { useContext } from "react";
import MediaCard from "./Cards";
import LessonAlert from "./lessonAlert";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function CustomizedDialogs(props) {
  // console.log(props.func);
  // console.log(props.id);
  const NextLesson = props.id + 1;
  const handleBack = props.func;
  // console.log("NextID below:");
  // console.log(NextLesson);
  const [schema, setSchema] = useContext(LessonsContext);
  const [open, setOpen] = React.useState(false);

  const schema2 = schema;

  // create showFinishAlert state
  const [showFinishAlert, setShowFinishAlert] = React.useState(false);

  // only runs when all quizzes are complete
  const handleClickOpen = () => {
    // if all values of quizzesDone are true, then set open to true
    if (schema.lessons[props.id].quizzesDone.every((x) => x === true)) {
      setOpen(true);
      schema2.lessons[props.id].isDone = true;
      setSchema(schema2);
      localStorage.setItem("schema", JSON.stringify(schema2));
    } else {
      // Not all the answers are correct
      setShowFinishAlert(true);
      //set time out to show alert for 3 seconds
      setTimeout(() => {
        setShowFinishAlert(false);
      }, 3000);
    }
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div className={"FinishLessonSection"}>
        <div className="FinishAlert">
          {showFinishAlert && (
            <LessonAlert
              success={false}
              incorrectAnswerText="Please answer all questions correctly"
            />
          )}
        </div>
        <span className="btn btn-primary" onClick={handleClickOpen}>
          Finish Lesson
        </span>
        <span className="btn" onClick={handleBack}>
          Back
        </span>
      </div>
      <div className="fButton">
        <BootstrapDialog
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <BootstrapDialogTitle
            id="customized-dialog-title"
            onClose={handleClose}
            style={{
              margin: "20px",
              textAlign: "center",
            }}
          >
            {NextLesson !== 3
              ? "Congratulations! you have completed the lesson!"
              : "Thanks for Completing the Demo!"}
          </BootstrapDialogTitle>
          <DialogContent dividers className="fullBox">
            <Typography component={"div"}>
              {/* Don't render the card when NextLesson is lesson 4 */}
              {NextLesson !== 3 && (
                <div className="LHS">
                  <MediaCard
                    key={schema.lessons[NextLesson].id}
                    Image={schema.lessons[NextLesson].image}
                    AltImgtxt={schema.lessons[NextLesson].altImgtxt}
                    heading={schema.lessons[NextLesson].title}
                    body={schema.lessons[NextLesson].description}
                    LsnLink={
                      "/lessons/" +
                      schema.lessons[NextLesson].module +
                      "/" +
                      schema.lessons[NextLesson].urlParam
                    }
                    progress={
                      schema.lessons[NextLesson].isDone
                        ? 100
                        : (schema.lessons[NextLesson].currentIndex /
                            schema.lessons[NextLesson].lessonContent.length) *
                          100
                    }
                  ></MediaCard>
                </div>
              )}

              <Link
                to="/"
                className={NextLesson !== 3 ? "RHS BOX" : "RHSALT BOX"}
              >
                <div>
                  <HomeIcon fontSize="inherit" sx={{ scale: "4" }} />
                  <br />
                  <br />
                  <br />
                  Back to Dashboard
                </div>
              </Link>
            </Typography>
          </DialogContent>
        </BootstrapDialog>
      </div>
    </>
  );
}
