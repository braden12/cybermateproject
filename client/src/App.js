import React, { Component } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ActivateLayout from "./pages/activateLayout";
import ActivateEmailLayout from "./pages/activateEmailLayout";
import PrivacyPolicy from "./pages/PrivacyPolicy";
import UpdateInfo from "./pages/updateInfo";

import {
  Signup,
  Login,
  AboutUs,
  Error,
  Home,
  Terms,
  Contact,
  Forgot,
  Dashboard,
  FAQ,
  LoggedInUserRepellant,
  ProtectedRoute,
  Lessons,
  LessonPage,
  ResetPage,
} from "./pages";

import { LessonsProvider } from "./context/lessonsContext"; // the context storing all the lessons data

class App extends Component {
  state = {};

  render() {
    return (
      //BrowserRouter syncs URL with pages
      <LessonsProvider>
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={
                <LoggedInUserRepellant>
                  <Home />
                </LoggedInUserRepellant>
              }
            />{" "}
            {/* "/" = default homepage */}
            <Route
              path="/signup"
              element={
                <LoggedInUserRepellant>
                  <Signup />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/login"
              element={
                <LoggedInUserRepellant>
                  <Login />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/aboutus"
              element={
                <LoggedInUserRepellant>
                  <AboutUs />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/terms"
              element={
                <LoggedInUserRepellant>
                  <Terms />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/privacypolicy"
              element={
                <LoggedInUserRepellant>
                  <PrivacyPolicy />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/contact"
              element={
                <LoggedInUserRepellant>
                  <Contact />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/Forgot"
              element={
                <LoggedInUserRepellant>
                  <Forgot />
                </LoggedInUserRepellant>
              }
            />
            <Route
              path="/UpdateInfo"
              element={
                <ProtectedRoute>
                  <UpdateInfo />
                </ProtectedRoute>
              }
            />
            <Route
              path="/FAQ"
              element={
                <LoggedInUserRepellant>
                  <FAQ />
                </LoggedInUserRepellant>
              }
            />
            <Route path="*" element={<Error />} />
            <Route
              path="/dashboard"
              element={
                <ProtectedRoute>
                  <Dashboard />
                </ProtectedRoute>
              }
            />
            <Route path="/lessons">
              <Route
                index
                element={
                  <ProtectedRoute>
                    <Lessons />
                  </ProtectedRoute>
                }
              />
              <Route
                path=":moduleID/:lessonID"
                element={
                  <ProtectedRoute>
                    <LessonPage />
                  </ProtectedRoute>
                }
                key={() => {
                  Math.random();
                }}
              />
            </Route>
            <Route
              path="/api/v1/auth/activate/:activation_token"
              exact
              element={<ActivateLayout />}
            />
            <Route
              path="/auth/reset-password/:activation_token"
              exact
              element={<ResetPage />}
            />
            <Route
              path="/api/v1/auth/gg-email/:activation_token"
              exact
              element={<ActivateEmailLayout />}
            />
          </Routes>
        </BrowserRouter>
      </LessonsProvider>
    );
  }
}

export default App;
