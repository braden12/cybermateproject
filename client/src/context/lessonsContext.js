// common state for all lessons. All child components will be able to access this data

import { createContext, useState } from "react";

const LessonsContext = createContext();

// function for passing values to the children of the context
export function LessonsProvider({ children }) {
  // function that runs once on startup to get schema from localstorage
  var localData = localStorage.getItem("schema");
  {
    localData
      ? (localData = JSON.parse(localData))
      : (localData = lessonSchema);
  }

  // function that runs once on startup to get counter from localstorage
  var localData2 = localStorage.getItem("counter");
  {
    localData2 ? (localData2 = JSON.parse(localData2)) : (localData2 = 0);
  }

  // create schema to be passed down
  const [schema, setSchema] = useState(localData);

  // create counter for holding stepper index values
  const [counter, setCounter] = useState(localData2);

  return (
    <LessonsContext.Provider value={[schema, setSchema, counter, setCounter]}>
      {children}
    </LessonsContext.Provider>
  );
}

// default schema to be loaded on first load
const lessonSchema = {
  lessons: [
    {
      id: 0, // unique identifier for each lesson, used to dynamically load the correct lesson
      image: "/static/CardImages/Cyber1.jpg", // image to be displayed on the lesson card
      altImgText: "CyberSecurity1",
      urlParam: "computers-and-software", // url parameter for the lesson, used to dynamically load the correct lesson
      module: "threat-landscape", // module that the lesson belongs to, used to dynamically load the correct module
      title: "Lesson 1: Computers and Software",
      description:
        "Learn about what computers and software are and how to identify them.",
      lessonContent: [
        // array of headings for each step of the lesson
        {
          contentHeading: "Lesson Introduction",
        },
        {
          contentHeading: "What is a Computer?",
        },
        {
          contentHeading: "Hardware vs Software",
        },
        {
          contentHeading: "Different Types of Computers",
        },
        {
          contentHeading: "Check Your Operating System",
        },
        {
          contentHeading: "Quiz",
        },
      ],
      quizzesDone: [false, false, false], // array of booleans to track which quizzes have been completed
      quizAnswers: [], // for storing the user checked answers
      quizAppearanceVariables: [], // for storing the state results of answers (alerts, reset button etc.)
      isDone: false, // boolean to track if the lesson has been completed
      currentIndex: 0, // index of the current step
    },
    {
      id: 1,
      image: "/static/CardImages/Cyber2.jpg",
      altImgText: "CyberSecurity2",
      urlParam: "introducing-malware",
      module: "threat-landscape",
      title: "Lesson 2: Introducing Malware",
      description: "Common threats in cyberspace",
      lessonContent: [
        {
          contentHeading: "Lesson Introduction",
        },
        {
          contentHeading: "What is Malware",
        },
        {
          contentHeading: "Types of Malware",
        },
        {
          contentHeading: "How Malware Spreads",
        },
        {
          contentHeading: "Signs and Symptoms of Infection",
        },
        {
          contentHeading: "Protecting against Malware",
        },
        {
          contentHeading: "Quiz",
        },
      ],
      quizzesDone: [false, false, false, false],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 2,
      image: "/static/CardImages/Cyber3.jpg",
      altImgText: "CyberSecurity3",
      urlParam: "phishing",
      module: "threat-landscape",
      title: "Lesson 3: Phishing",
      description: "The most common threat and the beginning of most attacks",
      lessonContent: [
        {
          contentHeading: "Lesson Introduction",
        },
        {
          contentHeading: "What is phishing?",
        },
        {
          contentHeading: "Common types of phishing",
        },
        {
          contentHeading: "Identifying phishing",
        },
        {
          contentHeading: "Protecting yourself from phishing",
        },
        {
          contentHeading: "What to do if you've been successfully phished",
        },
        {
          contentHeading: "Quiz",
        },
      ],
      quizzesDone: [false, false, false, false],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 3,
      image: "/static/CardImages/Cyber4.jpg",
      altImgText: "CyberSecurity4",
      urlParam: "",
      module: "threat-landscape",
      title: "Lesson 4: Networking and the Internet",
      description:
        "Learn about how computer networks work and how they form the internet",
      lessonContent: [
        {
          contentHeading: "Lesson Introduction",
        },
        {
          contentHeading: "What is malware",
        },
        {
          contentHeading: "How Malware Spreads",
        },
        {
          contentHeading: "The harm malware can cause",
        },
        {
          contentHeading: "Malware protection on your Operating System",
        },
      ],
      quizzesDone: [false, false, false, false],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },

    {
      id: 4,
      image: "/static/CardImages/Cyber5.jpg",
      altImgText: "CyberSecurity5",
      urlParam: "",
      module: "threat-landscape",
      title: "Lesson 5: Updates",
      description: "The role of updates in securing systems",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 5,
      image: "/static/CardImages/Cyber7.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "threat-landscape",
      title: "Lesson 6: Passwords",
      description: "The basics of having a secured password",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 6,
      image: "/static/CardImages/Cyber6.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "threat-landscape",
      title: "Lesson 7: MFA",
      description: "Multi-factor authentication",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },

    {
      id: 7,
      image: "/static/CardImages/email.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 1: Email",
      description: "Securing your digital address and ensuring best practice",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 8,
      image: "/static/CardImages/socialMedia.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 2: Social Media",
      description: "How to use social media safely and securely",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 9,
      image: "/static/CardImages/mobileDevices.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 3: Mobile Devices",
      description: "Specialised security for mobile devices",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 10,
      image: "/static/CardImages/backup.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 4: Backups",
      description: "How to backup your data and why it is important",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 11,
      image: "/static/CardImages/Cyber8.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 5: VPN",
      description: "What is a VPN, why they are used and how to use one",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 12,
      image: "/static/CardImages/antiVirus.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 6: Anti-Virus & Anti-Malware",
      description: "What is anti-virus and anti-malware and how to use them",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 13,
      image: "/static/CardImages/ifYouGetHacked.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "Building-Digital-Defences",
      title: "Lesson 7: If you get hacked",
      description: "If you get hacked, what process to follow",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },

    {
      id: 14,
      image: "/static/CardImages/recon.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 1: Reconnaissance",
      description: "Searching for information about a target",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 15,
      image: "/static/CardImages/weaponisation.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 2: Weaponisation",
      description:
        "Crafting a weapon to attack a target using the information gathered",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 16,
      image: "/static/CardImages/delivery.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 3: Delivery",
      description: "How to deliver the weapon to the target",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 17,
      image: "/static/CardImages/exploitation.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 4: Exploitation",
      description: "How to exploit the target using the weapon",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 18,
      image: "/static/CardImages/installation.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 5: Installation",
      description: "Installing the weapon on the target",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 19,
      image: "/static/CardImages/commandAndControl.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 6: Command & Control",
      description: "Controlling the weapon once it is installed",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
    {
      id: 20,
      image: "/static/CardImages/actionsOnObjectives.jpg",
      altImgText: "CyberSecurity6",
      urlParam: "",
      module: "How-Hackers-Think",
      title: "Lesson 7: Actions on Objectives",
      description: "Using the weapon to achieve the objective",
      lessonContent: [{}],
      quizzesDone: [true],
      quizAnswers: [], // for storing the user checked answers
      isDone: false,
      currentIndex: 0,
    },
  ],
};

export default LessonsContext;
