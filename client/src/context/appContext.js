import React, { useReducer, useContext } from "react";
import reducer from "./reducer";
import axios from "axios";

// Document for the ..state of the application.
// All global variables and functions are stored here
// paired with reducers to change the app state

// All reducer "actions" to be dispatched and handled in reducer.js
import {
  DISPLAY_ALERT,
  CLEAR_ALERT,
  REGISTER_USER_BEGIN,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  LOGIN_USER_BEGIN,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGOUT_USER,
  ACTIVATE_USER,
  FORGOT_USER,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAIL,
  EMAIL_SENT,
  EMAIL_DUPLICATE,
  //DETAILS_SAVED,
} from "./actions";

// initialise the user data and token
const token = localStorage.getItem("token");
const user = localStorage.getItem("user");

// initialise initial state
const initialState = {
  isLoading: false,
  showAlert: false,
  alertText: "",
  alertType: "",
  user: user ? JSON.parse(user) : null,
  token: token,
};

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  // for all elements (children) AppProvider is placed around (see index.js)

  // utilise useReducer() hook to allow the alteration of initialState values with "dispatch"
  const [state, dispatch] = useReducer(reducer, initialState);

  // displays values in alertText in accordance with alertType. See reducer.js DISPLAY_ALERT
  const displayAlert = () => {
    dispatch({ type: DISPLAY_ALERT });
    clearAlert();
  };

  // removes the alert after x seconds
  const clearAlert = () => {
    setTimeout(() => {
      dispatch({
        type: CLEAR_ALERT,
      });
    }, 8000);
  };

  // for password checking
  const displayPassAlert = (type) => {
    dispatch({ type: type });
    clearAlert();
  };

  // Adds user and JWT token to localstorage in the browser so the user can continue their session if they didn't log out.
  const addUserToLocalStorage = ({ user, token }) => {
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("token", token);
  };

  // removes the user and JWT token from storage. Effectively logs the user out.
  const removeUserFromLocalStorage = () => {
    localStorage.removeItem("user", user);
    localStorage.removeItem("token", token);
  };

  // function to create an "activate user" email
  const registerUser = async (currentUser) => {
    dispatch({ type: REGISTER_USER_BEGIN }); // used to update the context/state
    try {
      await axios.post("/api/v1/auth/signup", currentUser); // see authController.js signup

      dispatch({ type: REGISTER_USER_SUCCESS, payload: { user: currentUser } }); // Updates the alert in appContext
    } catch (error) {
      //console.log(error.response);
      dispatch({
        type: REGISTER_USER_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };

  // function for activating a user
  const ActivateUser = async (activation_token) => {
    try {
      // Create a post request with activation token to create an account
      const res = await axios.post("/api/v1/auth/activate", {
        activation_token,
      });

      //get user and JSON Web Token from response
      const { user, token } = res.data;

      // Updates appcontext State
      dispatch({
        type: ACTIVATE_USER,
        payload: {
          user,
          token,
        },
      });

      //Add the user to local storage with token
      addUserToLocalStorage({ user, token });
    } catch (err) {
      console.log(err);
    }
  }; //end of activate user

  const loginUser = async (currentUser) => {
    dispatch({ type: LOGIN_USER_BEGIN });
    try {
      const { data } = await axios.post("/api/v1/auth/login", currentUser);

      const { user, token } = data;
      dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: {
          user,
          token,
        },
      });
      addUserToLocalStorage({ user, token });
    } catch (error) {
      dispatch({
        type: LOGIN_USER_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };

  const logoutUser = () => {
    dispatch({ type: LOGOUT_USER });
    removeUserFromLocalStorage();
  };

  const updateInfo = async (currentUser, activation_token) => {
    const headers = {
      "content-type": "application/json",
      authorization: `${activation_token}`,
    };
    //console.log("Current User");
    //console.log(currentUser);
    //console.log(activation_token);
    await axios.patch(
      "/api/v1/auth/updateUser",
      { currentUser },
      { headers: headers }
    );
    //displayPassAlert(DETAILS_SAVED);

    //console.log(res);
  };

  // forgot user function
  const forgot = async (email) => {
    dispatch({
      type: FORGOT_USER,
    });
    //console.log("Email from appcont");
    //console.log(email);
    await axios.post("/api/v1/auth/forgot_pass", { email });

    clearAlert();
  };

  const Reset = async (password, activation_token) => {
    //console.log("App Context");
    //console.log(password);
    //console.log(activation_token);

    const headers = {
      "content-type": "application/json",
      authorization: `${activation_token}`,
    };

    const res = await axios.post(
      "/api/v1/auth/reset_pass",
      { password },
      { headers: headers }
    );
    //console.log(res.status);
    // if response successful
    if (res.status === 200) {
      dispatch({
        type: RESET_PASSWORD_SUCCESS,
      });
      clearAlert();
      return true;
    }
    // if response unsuccessful
    else {
      dispatch({
        type: RESET_PASSWORD_FAIL,
      });
      return false;
    }
  };

  const UpdateEmail = async (new_email) => {
    try {
      // Create a post request with activation token to create an account
      const user_info = JSON.parse(user);
      //console.log(user_info.email);
      const old_email = user_info.email;
      const fname = user_info.fname;
      const lname = user_info.lname;
      //console.log("Last Name");
      //console.log(lname);
      //console.log(new_email);
      await axios.post("/api/v1/auth/update_email", {
        fname,
        lname,
        old_email,
        new_email,
      });
      displayPassAlert(EMAIL_SENT);
    } catch (err) {
      if (err.response.status == 400) {
        displayPassAlert(EMAIL_DUPLICATE);
      }
      //console.log("This is res2");
      //console.log(err.response.status);
    }
  }; //end of ConfirmEmail

  const ConfirmEmail = async (activation_token) => {
    try {
      // Create a post request with activation token to create an account
      const res = await axios.post("/api/v1/auth/gg-email", {
        activation_token,
      });
      const user_info = JSON.parse(user);
      //console.log(res.data.msg);
      user_info["email"] = res.data.msg;
      localStorage.setItem("user", JSON.stringify(user_info));
    } catch (err) {
      //console.log(err);
    }
  }; //end of ConfirmEmail

  return (
    <AppContext.Provider
      value={{
        ...state,
        ActivateUser,
        displayAlert,
        registerUser,
        loginUser,
        logoutUser,
        forgot,
        Reset,
        displayPassAlert,
        updateInfo,
        ConfirmEmail,
        UpdateEmail,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useAppContext = () => {
  return useContext(AppContext);
};

export { AppProvider, initialState, useAppContext };
