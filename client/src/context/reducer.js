import {
  DISPLAY_ALERT,
  CLEAR_ALERT,
  REGISTER_USER_BEGIN,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  LOGIN_USER_BEGIN,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGOUT_USER,
  ACTIVATE_USER,
  FORGOT_USER,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAIL,
  PASSWORD_LENGTH_SHORT,
  PASSWORD_MISSING_FIELDS,
  PASSWORD_MISSING_CHARACTER,
  PASSWORD_NOT_MATCHING,
  EMAIL_SENT,
  DETAILS_SAVED,
  EMAIL_DUPLICATE,
  FNAME_LENGTH_SHORT,
  LNAME_LENGTH_SHORT,
  FNAME_LENGTH_LONG,
  LNAME_LENGTH_LONG,
  FORGOT_USER_EMPTY,
} from "./actions";

import { initialState } from "./appContext";

const reducer = (state, action) => {
  if (action.type === DISPLAY_ALERT) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Please provide all values",
    };
  }
  if (action.type === CLEAR_ALERT) {
    return {
      ...state,
      showAlert: false,
      alertType: "",
      alertText: "",
    };
  }
  if (action.type === REGISTER_USER_BEGIN) {
    return {
      ...state,
    };
  }
  if (action.type === REGISTER_USER_SUCCESS) {
    return {
      ...state,
      showAlert: true,
      alertType: "success",
      alertText: "A Confirmation Email Has Been Sent!",
    };
  }
  if (action.type === ACTIVATE_USER) {
    return {
      ...state,
      user: action.payload.user, // This updates the state
      token: action.payload.token,
    };
  }
  if (action.type === REGISTER_USER_ERROR) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: action.payload.msg,
    };
  }

  if (action.type === LOGIN_USER_BEGIN) {
    return {
      ...state,
    };
  }
  if (action.type === LOGIN_USER_SUCCESS) {
    return {
      ...state,
      user: action.payload.user,
      token: action.payload.token,
      showAlert: true,
      alertType: "success",
      alertText: "Login Successful! Redirecting...",
    };
  }
  if (action.type === LOGIN_USER_ERROR) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: action.payload.msg,
    };
  }
  if (action.type === LOGOUT_USER) {
    return {
      ...initialState,
      user: null,
      token: null,
    };
  }
  if (action.type === FORGOT_USER) {
    return {
      ...state,
      showAlert: true,
      alertType: "success",
      alertText: "A Confirmation Email Has Been Sent!",
    };
  }
  if (action.type === RESET_PASSWORD_SUCCESS) {
    return {
      ...state,
      showAlert: true,
      alertType: "success",
      alertText: "Password reset, redirecting to login",
    };
  }
  if (action.type === PASSWORD_LENGTH_SHORT) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Please provide an 8 character password",
    };
  }
  if (action.type === PASSWORD_MISSING_FIELDS) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Please enter your password in BOTH fields",
    };
  }
  if (action.type === PASSWORD_MISSING_CHARACTER) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText:
        "Your password must contain at least one Upper-Case, Lower-Case, Number and Special character e.g. #*(@%",
    };
  }
  if (action.type === PASSWORD_NOT_MATCHING) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Your passwords do not match",
    };
  }
  if (action.type === RESET_PASSWORD_FAIL) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Password invalid",
    };
  }
  if (action.type === EMAIL_SENT) {
    return {
      ...state,
      showAlert: true,
      alertType: "success",
      alertText: "A confirmation email has been sent",
    };
  }
  if (action.type === DETAILS_SAVED) {
    return {
      ...state,
      showAlert: true,
      alertType: "success",
      alertText: "Your details have been saved",
    };
  }
  if (action.type === EMAIL_DUPLICATE) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "This email is already in use",
    };
  }
  if (action.type === FNAME_LENGTH_SHORT) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "First name too short",
    };
  }
  if (action.type === LNAME_LENGTH_SHORT) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Last name too short",
    };
  }
  if (action.type === FNAME_LENGTH_LONG) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "First name too long",
    };
  }
  if (action.type === LNAME_LENGTH_LONG) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Last name too long",
    };
  }
  if (action.type === FORGOT_USER_EMPTY) {
    return {
      ...state,
      showAlert: true,
      alertType: "danger",
      alertText: "Please provide an email",
    };
  }
  throw new Error(`no such action : ${action.type}`);
};
export default reducer;
