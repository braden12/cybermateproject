import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import About from "../components/Body(AboutUs)/About";

function AboutUs() {
  return (
    <div>
      <div className="full-page">
        <NavBar />
        <About />
      </div>
      <Footer />
    </div>
  );
}
export default AboutUs;
