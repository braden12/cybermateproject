import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Homebody from "../components/Body(Home)/Homebody";
import "./Home.css";

function Home() {
  return (
    <div>
      <div className="full-page">
        <NavBar />
        <Homebody />
      </div>
      <Footer />
    </div>
  );
}
export default Home;
