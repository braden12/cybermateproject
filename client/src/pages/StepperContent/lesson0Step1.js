// What is a Computer?
function Lesson0Step1() {
  return (
    <div>
      <p>
        <b>A computer</b> is an electronic device that can <b>retrieve</b>,
        <b> process</b> and <b>store</b> data. They are usually able to
        <b> communicate</b> with each other via networks.
      </p>
      <p>
        You may already know that you can use a computer to type documents, send
        email, play games, and browse the Internet. You can also use them to
        edit or create spreadsheets, presentations, and even videos.
      </p>

      <p>
        These kinds of computers are more specifically called personal
        computers.
      </p>
      <p>
        If it's an electronic device that stores and processes data, it can be
        considered a computer
      </p>
      <p>
        Calculators, phones, digital alarm clocks are all computers. The modern
        world is full of them!
      </p>

      {/* youtube video */}
      <iframe
        src="https://www.youtube.com/embed/Cu3R5it4cQs?rel=0"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
    </div>
  );
}

export default Lesson0Step1;
