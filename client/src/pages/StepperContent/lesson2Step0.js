// Lesson Introdution
function Lesson2Step0() {
  return (
    <div>
      <b>What will be covered in this lesson:</b>

      <ul>
        <li>What Is Phishing</li>
        <li>Common Types Of Phishing</li>
        <li>Identifying Phishing</li>
        <li>Protecting Yourself From Phishing</li>
        <li>What To Do If You've Been Phished</li>
      </ul>
    </div>
  );
}

export default Lesson2Step0;
