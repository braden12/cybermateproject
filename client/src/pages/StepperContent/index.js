import Lesson0Step0 from "./lesson0Step0";
import Lesson0Step1 from "./lesson0Step1";
import Lesson0Step2 from "./lesson0Step2";
import Lesson0Step3 from "./lesson0Step3";
import Lesson0Step4 from "./lesson0Step4";
import Lesson0Step5 from "./lesson0Step5";
import Lesson0Step6 from "./lesson0Step6";

import Lesson1Step0 from "./lesson1Step0";
import Lesson1Step1 from "./lesson1Step1";
import Lesson1Step2 from "./lesson1Step2";
import Lesson1Step3 from "./lesson1Step3";
import Lesson1Step4 from "./lesson1Step4";
import Lesson1Step5 from "./lesson1Step5";
import Lesson1Step6 from "./lesson1Step6";

import Lesson2Step0 from "./lesson2Step0";
import Lesson2Step1 from "./lesson2Step1";
import Lesson2Step2 from "./lesson2Step2";
import Lesson2Step3 from "./lesson2Step3";
import Lesson2Step4 from "./lesson2Step4";
import Lesson2Step5 from "./lesson2Step5";
import Lesson2Step6 from "./lesson2Step6";

export {
  Lesson0Step0,
  Lesson0Step1,
  Lesson0Step2,
  Lesson0Step3,
  Lesson0Step4,
  Lesson0Step5,
  Lesson0Step6,
  Lesson1Step0,
  Lesson1Step1,
  Lesson1Step2,
  Lesson1Step3,
  Lesson1Step4,
  Lesson1Step5,
  Lesson1Step6,
  Lesson2Step0,
  Lesson2Step1,
  Lesson2Step2,
  Lesson2Step3,
  Lesson2Step4,
  Lesson2Step5,
  Lesson2Step6,
};

// export { default as Lesson0Step0 } from "./lesson0Step0";
// export { default as Lesson0Step1 } from "./lesson0Step1";
// export { default as Lesson0Step2 } from "./lesson0Step2";
// export { default as Lesson0Step3 } from "./lesson0Step3";
// export { default as Lesson0Step4 } from "./lesson0Step4";
// export { default as Lesson0Step5 } from "./lesson0Step5";
// export { default as Lesson0Step6 } from "./lesson0Step6";

// export { default as Lesson1Step0 } from "./lesson1Step0";
// export { default as Lesson1Step1 } from "./lesson1Step1";
// export { default as Lesson1Step2 } from "./lesson1Step2";
// export { default as Lesson1Step3 } from "./lesson1Step3";
// export { default as Lesson1Step4 } from "./lesson1Step4";
// export { default as Lesson1Step5 } from "./lesson1Step5";
// export { default as Lesson1Step6 } from "./lesson1Step6";

// export { default as Lesson2Step0 } from "./lesson2Step0";
// export { default as Lesson2Step1 } from "./lesson2Step1";
// export { default as Lesson2Step2 } from "./lesson2Step2";
// export { default as Lesson2Step3 } from "./lesson2Step3";
// export { default as Lesson2Step4 } from "./lesson2Step4";
// export { default as Lesson2Step5 } from "./lesson2Step5";
// export { default as Lesson2Step6 } from "./lesson2Step6";
