function Lesson2Step3() {
  return (
    <div>
      <b>Protecting yourself from phishing </b>
        <p>
        Educating yourself and staff is the most important way to protect yourself or your organisation from a phishing attack. 
        Knowing what an attack looks like by always be on the lookout for fake links or attachments is key to protecting yourself. 
        Other ways are not providing sensitive information to unverified sources, 
        enabling multi-factor authentication and trying to keep up with the latest news on phishing attacks.
        </p>
    </div>
  );
}

export default Lesson2Step3;
