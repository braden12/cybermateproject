// Lesson Introdution
function Lesson1Step0() {
  return (
    <div>
      <b>What will be covered in this lesson:</b>

      <ul>
        <li>What is Malware</li>
        <li>Types of Malware</li>
        <li>How Malware Spreads</li>
        <li>Signs and Symptoms of Infection</li>
        <li>How to protect against Malware</li>
      </ul>
    </div>
  );
}

export default Lesson1Step0;
