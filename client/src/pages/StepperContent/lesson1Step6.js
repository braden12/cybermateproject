// import checkboxes
import Checkboxes from "../../components/Checkboxes";
const question0 = "Which of the following are ways Malware can spread?";

const answerList0 = [
  {
    name: "A",
    answer: "Email attachments",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "B",
    answer: "Visiting a malicious website",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "C",
    answer: "Downloading a file from a website",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: "Plugging an infected USB drive into your computer",
    Num: 3,
    correctAnswer: true,
  },
];

const question1 = "What makes a virus a virus?";

const answerList1 = [
  {
    name: "A",
    answer: "It can replicate itself",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: "It can infect other devices",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "B",
    answer: "It gives attackers remote access to your computer",
    Num: 2,
    correctAnswer: false,
  },
  {
    name: "C",
    answer: "It can see what keys you press",
    Num: 3,
    correctAnswer: false,
  },
];

const question2 = "What are signs your device has malware?";

const answerList2 = [
  {
    name: "one",
    answer: "Applications take longer to open and to respond",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "two",
    answer: "Your operating system prompts you to install an update",
    Num: 1,
    correctAnswer: false,
  },
  {
    name: "three",
    answer: "Unwanted ads start appearing where there were none before",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "four",
    answer: "Error messages appear and programs crash",
    Num: 3,
    correctAnswer: true,
  },
];

const question3 = "Which of the following protect your computer from malware?";

const answerList3 = [
  {
    name: "one",
    answer: "Using a VPN (Virtual Private Network) to hide your IP address",
    Num: 0,
    correctAnswer: false,
  },
  {
    name: "two",
    answer: "Keeping your Operating System (OS) and Software up to date",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "three",
    answer: "Only using trusted websites and legitimate software",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "four",
    answer: "Antivirus and anti-malware software",
    Num: 3,
    correctAnswer: true,
  },
];

const incorrectAnswerText0 = "Incorrect answer, please try again";
const correctAnswerText0 =
  "Well done! If data is being transmitted into your computer, that data could be malware.";

const incorrectAnswerText1 = "Incorrect answer, please try again";
const correctAnswerText1 =
  "Well done! The most common type of malware is a virus, with trojans and worms being examples";

const incorrectAnswerText2 = "Incorrect answer, please try again";
const correctAnswerText2 = "Correct answer, well done!";

const incorrectAnswerText3 =
  "Incorrect answer. A tip: Hiding your IP address does not help against malware.";
const correctAnswerText3 =
  "Well done! A VPN is a good way to protect your privacy, but it won't protect you from malware";

function Lesson1Step5() {
  return (
    <div>
      Please Note:
      <ul>
        <li>Questions are multiple choice.</li>
        <li>You must select all correct answers.</li>
        <li>You can select more than one answer.</li>
      </ul>
      <br />
      <Checkboxes
        question={question0}
        answerList={answerList0}
        incorrectAnswerText={incorrectAnswerText0}
        correctAnswerText={correctAnswerText0}
        lessonNumber={1}
        quizNumber={0}
      />
      <Checkboxes
        question={question1}
        answerList={answerList1}
        incorrectAnswerText={incorrectAnswerText1}
        correctAnswerText={correctAnswerText1}
        lessonNumber={1}
        quizNumber={1}
      />
      <Checkboxes
        question={question2}
        answerList={answerList2}
        incorrectAnswerText={incorrectAnswerText2}
        correctAnswerText={correctAnswerText2}
        lessonNumber={1}
        quizNumber={2}
      />
      <Checkboxes
        question={question3}
        answerList={answerList3}
        incorrectAnswerText={incorrectAnswerText3}
        correctAnswerText={correctAnswerText3}
        lessonNumber={1}
        quizNumber={3}
      />
    </div>
  );
}

export default Lesson1Step5;
