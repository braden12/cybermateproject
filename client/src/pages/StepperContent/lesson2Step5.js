function Lesson2Step4() {
  return (
    <div>
      <b>What to do if you’ve been successfully phished </b>
        <p>
        Record the details of the attack, 
        what you may have clicked on, where the message came from or what sensitive information was shared are important details that might be relevant. 
        After that change all passwords of the accounts that might be affected. 
        Contact the relevant organisations that the attack involved, 
        for example if bank details were stolen then contact your bank or if your work or school account was compromised contact your work or school. 
        If money has been stolen or you’ve become a victim of identity theft, then report it to the local law enforcement. 
        </p>
    </div>
  );
}

export default Lesson2Step4;
