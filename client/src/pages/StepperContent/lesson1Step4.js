function Lesson1Step3() {
  return (
    <div>
      <ol type="1">
        <li>
          Device is suddenly running slow, crashes, heats up or displays error
          messages.
        </li>
        <li>Device won't shut down or restart. </li>
        <li>Will not let you remove software. </li>
        <li>
          Lots of pop-ups, inappropriate ads, or ads that interfere with page
          content.
        </li>
        <li>
          Your contacts are receiving strange messages and emails you did not
          send.
        </li>
        <li>Programs keep crashing. </li>
        <li>Programs take longer than usual to load. </li>
        <li>Curser moves all by itself. </li>
      </ol>
    </div>
  );
}

export default Lesson1Step3;
