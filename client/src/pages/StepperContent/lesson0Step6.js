// This is a placeholder document as this document is required to exist but will not be rendered by stepper.js
function Lesson0Step6() {
  return (
    <div>If you're seeing this, please report it at cycademy@gmail.com</div>
  );
}

export default Lesson0Step6;
