// Different Types of Computers

function Lesson0Step3() {
  return (
    <div>
      <p>
        Computers come in many different shapes and sizes. They can be as small
        as a watch or as large as a room. They can be as simple as a{" "}
        <b>calculator </b>
        doing arithmetic or as complex as a <b>supercomputer</b> running
        chemical simulations.
      </p>

      <p>
        The following are some of the most common types of computers you will
        encounter.
      </p>
      <p>
        <b>Desktop Computers</b>, which represent computers that are usually in
        a fixed place at work, home or school. They are usually at a desk and
        often come with a separate screen, keyboard and mouse.{" "}
      </p>
      <img
        src={"/static/images/Desktop.jpg"}
        alt="Desktop Computer"
        className="LsnImgResizer"
      />
      <p>
        <b>Laptop Computers</b>, or laptops, are battery powered computers
        designed to be portable. They usually come with an integrated (built in)
        keyboard, screen and mousepad.
      </p>
      <img
        src="/static/images/DellLaptop.jpg"
        alt="Laptop Computer"
        className="LsnImgResizer"
      />

      <p>
        <b>Tablets</b> are handheld computers with touchscreens for typing and
        navigation. The Apple iPad is an example of a tablet.
      </p>
      <img
        src="/static/images/Tablet.jpg"
        alt="Tablet Computer"
        className="LsnImgResizer"
      />

      <p>
        <b>Smartphones</b> are handheld computers with touch screens for typing
        and navigation. They can also make phone calls. The most common phones
        use Apple IOS or Android operating systems.
      </p>
      <img
        src="/static/images/Cyber5.jpg"
        alt="Smartphone Computer"
        className="LsnImgResizer"
      />

      <p>
        <b>Internet Routers</b> are small computers that connect multiple
        devices to the internet. They are often used in homes and businesses to
        connect multiple computers to the internet.
      </p>
      <img
        src="/static/images/Router.jpg"
        alt="Internet Router"
        className="LsnImgResizer"
      />
      <p>
        <b>Smart Watches</b> are small computers that connect to the internet
        and can be used to make phone calls, send text messages, and access the
        internet. They are worn on the wrist.
      </p>

      <img
        src="/static/images/Smartwatch.jpg"
        alt="Smart Watch"
        className="LsnImgResizer"
      />

      <p>Other devices with an integrated computer are:</p>
      <ul>
        <li>Printers</li>
        <li>Projectors</li>
        <li>Video Game Consoles</li>
        <li>Smart TVs</li>
        <li>Smart Fridges</li>
        <li>Smart Cars</li>
        <li>Smart Speakers such as Amazon Echo and Google Home</li>
      </ul>
    </div>
  );
}

export default Lesson0Step3;
