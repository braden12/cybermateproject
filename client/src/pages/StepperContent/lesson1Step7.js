// This is a placeholder document as this document is required to exist but will not be rendered by stepper.js
function Lesson1Step6() {
  return (
    <div>
      Content <b>goes</b> <i>in here </i>
      Along with everything else if needed.
      {/* bullet list */}
      <ul>
        <li>Item 1</li>
        <ul>
          <li>Item 1.1</li>
          <li>Item 1.2</li>
        </ul>
        <li>Item 2</li>
        <li>Item 3</li>
      </ul>
    </div>
  );
}

export default Lesson1Step6;
