function Lesson1Step0() {
  return (
    <div>
      <p>
        Any intrusive software developed by cybercriminals to steal data and
        damage or destroy computers and computer systems. Examples of common
        malware include viruses, worms, Trojan viruses, spyware, adware, and
        ransomware.
      </p>

      {/* youtube video */}
      <iframe
        src="https://www.youtube.com/embed/AfZxUK9U3hE?rel=0start=0&end=98"
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
    </div>
  );
}

export default Lesson1Step0;
