function Lesson1Step1() {
  return (
    <div>
      <div>
        <b>Virus</b>
        <p>
          A computer virus is a type of computer program that, when executed,
          replicates itself by modifying other computer programs and inserting
          its own code.{" "}
        </p>
        <img
          src="/static/images/Virus.jpg"
          alt="Virus_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Worms</b>
        <p>
          Worms are malicious software that rapidly replicates and spreads to
          any device within the network.{" "}
        </p>
        <img
          src="/static/images/Worms.jpg"
          alt="Worms_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Trojan</b>
        <p>
          Trojan viruses are disguised as helpful software programs. But once
          the user downloads it, the Trojan virus can gain access to sensitive
          data and then modify, block, or delete the data.{" "}
        </p>
        <img
          src="/static/images/Trojan.jpg"
          alt="Trojan_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Spyware</b>
        <p>
          Spyware is malicious software that runs secretly on a computer and
          reports back to a remote user. Rather than simply disrupting a
          device's operations, spyware targets sensitive information and can
          grant remote access to predators.{" "}
        </p>
        <img
          src="/static/images/Spyware.jpg"
          alt="Spyware_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Adware</b>
        <p>
          Adware is any software application in which an advertising banner or
          other advertising material displays or downloads while a program is
          running.{" "}
        </p>
        <img
          src="/static/images/Adware.jpg"
          alt="Adware_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Ransomware</b>
        <p>
          Ransomware is a common and dangerous type of malware. It works by
          locking up or encrypting your files so you can no longer access them.{" "}
        </p>
        <img
          src="/static/images/Ransomware.jpg"
          alt="Ransomware_Img"
          className="LsnImgResizer"
        />
      </div>

      <div>
        <b>Fileless Malware</b>
        <p>
          Fileless malware is a variant of computer-related malicious software
          that exists exclusively as a computer memory-based artifact i.e., in
          RAM.
        </p>
        <img
          src="/static/images/Fileless_Malware.jpg"
          alt="Fileless_Malware_Img"
          className="LsnImgResizer"
        />
      </div>
    </div>
  );
}

export default Lesson1Step1;
