// import checkboxes
import Checkboxes from "../../components/Checkboxes";
// import context
import { useContext } from "react";
// import lessonsContext
import LessonsContext from "../../context/lessonsContext";

const question0 = "Which of the following is an example of software?";

const answerList0 = [
  {
    name: "A",
    answer: "An Operating System (OS)",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "B",
    answer: "A computer monitor",
    Num: 1,
    correctAnswer: false,
  },
  {
    name: "C",
    answer: "A computer virus",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: "An internet Browser",
    Num: 3,
    correctAnswer: true,
  },
];

const question1 = "Which of the following is an example of hardware?";

const answerList1 = [
  {
    name: "A",
    answer: "A mouse",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "B",
    answer: "A keyboard",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "C",
    answer: "A monitor",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: "A browser",
    Num: 3,
    correctAnswer: false,
  },
];

const question2 = "Which of the following are ALWAYS computers?";

const answerList2 = [
  {
    name: "one",
    answer: "iPhone",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "two",
    answer: "Keyboard",
    Num: 1,
    correctAnswer: false,
  },
  {
    name: "three",
    answer: "Internet Router",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "four",
    answer: "Earphones",
    Num: 3,
    correctAnswer: false,
  },
];

const incorrectAnswerText0 = "Incorrect answer, please try again";
const correctAnswerText0 = "Correct answer, well done!";

function Lesson0Step5() {
  return (
    <div>
      Please Note:
      <ul>
        <li>Questions are multiple choice.</li>
        <li>You must select all correct answers.</li>
        <li>You can select more than one answer.</li>
      </ul>
      <br />
      <Checkboxes
        question={question0}
        answerList={answerList0}
        incorrectAnswerText={incorrectAnswerText0}
        correctAnswerText={correctAnswerText0}
        lessonNumber={0}
        quizNumber={0}
      />
      <Checkboxes
        question={question1}
        answerList={answerList1}
        incorrectAnswerText={incorrectAnswerText0}
        correctAnswerText={correctAnswerText0}
        lessonNumber={0}
        quizNumber={1}
      />
      <Checkboxes
        question={question2}
        answerList={answerList2}
        incorrectAnswerText={incorrectAnswerText0}
        correctAnswerText={correctAnswerText0}
        lessonNumber={0}
        quizNumber={2}
      />
    </div>
  );
}

export default Lesson0Step5;
