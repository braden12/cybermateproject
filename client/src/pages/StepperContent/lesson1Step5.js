function Lesson1Step4() {
  return (
    <div>
      <p>
        Typically, businesses focus on preventative tools to stop breaches. By
        securing the perimeter, businesses assume they are safe. Some advanced
        malwares, however, will eventually make its way into your network.
      </p>
      <p>
        As a result, it is crucial to deploy technologies that continually
        monitor and detect malware that has evaded perimeter defenses.
        Sufficient advanced malware protection requires multiple layers of
        safeguards along with high-level network visibility and intelligence.
      </p>
    </div>
  );
}

export default Lesson1Step4;
