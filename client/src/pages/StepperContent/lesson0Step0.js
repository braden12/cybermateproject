// Lesson Introdution
function Lesson0Step0() {
  return (
    <div>
      <b>What will be covered in this lesson:</b>
      {/* bullet list */}
      <ul>
        <li>What is a computer?</li>
        <li>How hardware and software relate and differ</li>
        <li>The different kinds of computers</li>
        <li>How to find your operating system version on desktop</li>
      </ul>
    </div>
  );
}

export default Lesson0Step0;
