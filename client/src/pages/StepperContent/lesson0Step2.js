// Hardware vs Software
function Lesson0Step2() {
  return (
    <div>
      <p>
        <b>Hardware</b> is any part of your computer that has a physical
        structure.
      </p>
      <p>Examples of hardware include:</p>
      <ul>
        <li>
          Speakers
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/speaker.png"} alt="speaker" />
          </a>
        </li>
        <li>
          Screen
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/imac.png"} alt="IMac Screen" />
          </a>
        </li>
        <li>
          Keyboard
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/keyboard.png"} alt="keyboard" />
          </a>
        </li>
        <li>
          Mouse{" "}
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img
              src={"/static/Icons/computer-mouse.png"}
              alt="Computer-Mouse"
            />
          </a>
        </li>
      </ul>
      <p>It also includes all the computer's internal parts such as:</p>
      <ul>
        <li>
          Central Processing Unit (CPU)
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/microchip.png"} alt="CPU" />
          </a>
        </li>
        <li>
          Memory
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/hdd.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Cooling Fans
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/fan.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Power Supply Unit
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/power-supply.png"} alt="Computer-Mouse" />
          </a>
        </li>
      </ul>
      <p>
        <b>Software</b> is the set of instructions that tell the hardware what
        to do and how to work.
      </p>
      <p>Examples of software include:</p>
      <ul>
        <li>
          Operating systems
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/windows.png"} alt="Computer-Mouse" />
          </a>
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/apple.png"} alt="Computer-Mouse" />
          </a>
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/android.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Web browsers
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/chrome.png"} alt="Computer-Mouse" />
          </a>
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/edge.png"} alt="Computer-Mouse" />
          </a>
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/firefox.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Word processors
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/word.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Spreadsheets
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/excel.png"} alt="Computer-Mouse" />
          </a>
        </li>
        <li>
          Games
          <a
            href="https://www.flaticon.com/free-icons/speaker"
            title="Speaker icons created by Freepik - Flaticon"
          >
            <img src={"/static/Icons/chess.png"} alt="Computer-Mouse" />
          </a>
        </li>
      </ul>
      <p>
        Everything you do on your computer will rely on both hardware and
        software. For example, right now you may be viewing this lesson in a web
        browser (software) and using your mouse (hardware) to click from page to
        page.
      </p>
      <p>
        As you learn about different types of computers, ask yourself about the
        differences in their hardware. As you progress through this tutorial,
        you'll see that different types of computers also often use different
        types of software.
      </p>
    </div>
  );
}

export default Lesson0Step2;
