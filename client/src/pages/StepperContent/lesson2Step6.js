import Checkboxes from "../../components/Checkboxes";

const question0 =
  "Which of the following file types can potentially pose a danger to your machine when opened?";

const answerList0 = [
  {
    name: "B",
    answer: ".jpg used to store images",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "A",
    answer: ".exe used to run programs",
    Num: 1,
    correctAnswer: true,
  },

  {
    name: "D",
    answer: ".pdf & .docx - used to store documents",
    Num: 2,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: ".xslx, .xlsm, .xls etc. as they can be used to store spreadsheets",
    Num: 3,
    correctAnswer: true,
  },
];

const question1 =
  "Looking at the image above, What would imply it might be a Phishing Email?";

const answerList1 = [
  {
    name: "A",
    answer: "A: The email address seems suspicious",
    Num: 0,
    correctAnswer: true,
  },
  {
    name: "B",
    answer: "B: The email is urgent and requires you to act immediately",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "C",
    answer: "C: Bad Spelling and Grammar",
    Num: 2,
    correctAnswer: false,
  },

  {
    name: "D",
    answer: "D: The greeting is generic",
    Num: 3,
    correctAnswer: true,
  },
  {
    name: "D",
    answer: "E: The email requires you to click a link",
    Num: 4,
    correctAnswer: true,
  },
];

const question2 =
  "If you suspect an email is a phishing email, what should you do?";

const answerList2 = [
  {
    name: "one",
    answer: "Click the links to see if they are safe",
    Num: 0,
    correctAnswer: false,
  },
  {
    name: "two",
    answer:
      "Delete it immediately to prevent yourself from opening it in future",
    Num: 1,
    correctAnswer: true,
  },
  {
    name: "three",
    answer: "Reply to the sender to see if they are genuine",
    Num: 2,
    correctAnswer: false,
  },
  {
    name: "four",
    answer: "Report it",
    Num: 3,
    correctAnswer: true,
  },
  {
    name: "four",
    answer: "Don't open it",
    Num: 4,
    correctAnswer: true,
  },
];

const question3 =
  "Which of the following are safe to assume aren't phishing emails?";

const answerList3 = [
  {
    name: "A",
    answer: "Emails from your bank",
    Num: 0,
    correctAnswer: false,
  },
  {
    name: "B",
    answer: "Emails from your school",
    Num: 1,
    correctAnswer: false,
  },
  {
    name: "C",
    answer: "Emails or messages from your work",
    Num: 2,
    correctAnswer: false,
  },
  {
    name: "D",
    answer: "Emails, texts or messages from your friends",
    Num: 3,
    correctAnswer: false,
  },
  {
    name: "e",
    answer: "Emails, texts or messages from the police or government",
    Num: 3,
    correctAnswer: false,
  },
];

const incorrectAnswerText0 =
  "Incorrect. Almost any file type can be dangerous on opening";
const correctAnswerText0 =
  "Well Done! It is very rare but some .jpg files can contain viruses.";

const incorrectAnswerText1 = "Incorrect or incomplete answer, please try again";
const correctAnswerText1 = "Correct answer, well done!";

const incorrectAnswerText2 =
  "Incorrect or incomplete answer. Don't engage with malicious emails.";
const correctAnswerText2 =
  "Correct! Reporting should be done immediately and to the relevant authorities";

const incorrectAnswerText3 =
  "Incorrect. Organisations and people can be hacked to send phishing messages from legitimate sources. Be careful!";
const correctAnswerText3 =
  "Correct answer, well done! Anyone can be hacked and made to send phishing emails";

function Lesson2Step5() {
  return (
    <div>
      Please Note:
      <ul>
        <li>Questions are multiple choice.</li>
        <li>You must select all correct answers.</li>
        <li>You can select more than one answer.</li>
      </ul>
      <br />
      <Checkboxes
        question={question0}
        answerList={answerList0}
        incorrectAnswerText={incorrectAnswerText0}
        correctAnswerText={correctAnswerText0}
        lessonNumber={2}
        quizNumber={0}
      />
      <img
        className="PhishingImage"
        src={"/static/images/FakePhishingEmailFinal.png"}
        alt="phishing email"
      />
      <Checkboxes
        question={question1}
        answerList={answerList1}
        incorrectAnswerText={incorrectAnswerText1}
        correctAnswerText={correctAnswerText1}
        lessonNumber={2}
        quizNumber={1}
      />
      <Checkboxes
        question={question2}
        answerList={answerList2}
        incorrectAnswerText={incorrectAnswerText2}
        correctAnswerText={correctAnswerText2}
        lessonNumber={2}
        quizNumber={2}
      />
      <Checkboxes
        question={question3}
        answerList={answerList3}
        incorrectAnswerText={incorrectAnswerText3}
        correctAnswerText={correctAnswerText3}
        lessonNumber={2}
        quizNumber={3}
      />
    </div>
  );
}

export default Lesson2Step5;
