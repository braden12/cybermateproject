function Lesson2Step3() {
  return (
    <div>
      <b>Bad Spelling or Grammar</b>
      <p>
        Organisations will usually have high-quality and professional email
        messages sent out, if there are obvious spelling or grammatical errors
        there’s a chance that the message could be a scam.
      </p>
      <b>Urgent action </b>
      <p>
        Often a phishing message involves a consequence if urgent action is not
        taken so recipients are rushed to fall for the scam as they don’t have
        much time to spot flaws or inconsistencies with the message.
      </p>
      <b>Generic greetings </b>
      <p>
        Organisations will usually personalise emails with your name if you’ve
        registered with them, if there is a generic greeting such as “Dear sir
        or madam” it could be a sign that the message is a scam because scammers
        will send that same message to thousands or millions of email addresses
        so they’re not able to personalise the message very well.
      </p>
      <b>Suspicious Attachments </b>
      <p>
        If the message you’ve received contains any links or attachments that
        have an unfamiliar extension commonly associated with malware (.zip,
        .exe, .scr, etc.), or the address doesn’t match up with the
        organisation’s website then it’s a sign the message is a scam.
      </p>
      <b>Request of sensitive data </b>
      <p>
        Messages that request sensitive data such as login credentials or
        payment information always need to be treated with caution. You should
        make sure the sender of the message is 100% the legitimate sender and if
        there is a URL attached make sure it is 100% legitimate.
      </p>
    </div>
  );
}

export default Lesson2Step3;
