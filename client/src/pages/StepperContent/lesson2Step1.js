// What is phishing?
function Lesson2Step0() {
  return (
    <div>
      <p>
        Phishing is a type of <b>social engineering</b> in which the attacker
        will send communication usually through email, phone call or SMS in
        order to trick a person to steal sensitive information or to spread
        malicious software on the victim's system.
      </p>
      <iframe
        width="560"
        height="315"
        src="https://www.youtube-nocookie.com/embed/Y7zNlEMDmI4?start=7rel=0"
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
    </div>
  );
}

export default Lesson2Step0;
