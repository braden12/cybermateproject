function Lesson1Step2() {
  return (
    <div>
      <p>
        Each type of malware has its own way of causing havoc. Malware mostly
        relies on user action of some kind.
      </p>
      <p>
        Some are delivered over email via a link or executable file, others are
        delivered via instant messaging or social media.
      </p>
      <p>
        Even mobile phones are vulnerable to attack. It is essential that
        organizations are aware of all vulnerabilities so they can lay down an
        effective line of defense.
      </p>
    </div>
  );
}

export default Lesson1Step2;
