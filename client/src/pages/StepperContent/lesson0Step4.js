// Check Your Operating System
function Lesson0Step4() {
  return (
    <div>
      <p>
        The Operating System (OS) is the first piece of software installed on a
        computer. It will usually come preinstalled when you buy a device.
      </p>
      <p>
        Knowing what your operating system is, and what version you have, is
        essential for starting to address security on your device.
      </p>
      <p>
        The following is a quick guide to finding your Operating System on a
        personal computer. Please ignore any section that does not apply to your
        device(s)
      </p>
      <h4>On Windows:</h4>
      <p>
        Click the <b>Windows</b> button in the bottom left corner of your
        screen. Then type "System Information" to search for the System
        Information App.
      </p>
      <img
        src="/static/images/SearchBarWindows.png"
        alt="Windows Settings"
        className="LsnImgResizer"
      />
      <p>
        Open the System Information App, and your Operating System (OS) should
        be displayed at the top of the window.
      </p>
      <img
        src="/static/images/SystemSummary.png"
        alt="Windows About"
        className="LsnImgResizer"
      />

      <p>In this case the OS is Microsoft Windows 10 Education</p>
      <p>
        It's important to note that if your Windows Operating System is{" "}
        <b>version 7 or lower</b> that your system is at great risk and you
        should <b>upgrade immediately</b>. Older Windows OS no longer receive
        security updates and are prone to attacks.
      </p>

      <h4>On Mac:</h4>
      <p>
        Click the Apple icon in the top left corner of your screen. Then click
        About This Mac.
      </p>
      <img
        src="/static/images/MacAbout.jpg"
        alt="Mac About"
        className="LsnImgResizer"
      />
      <p>
        Your operating system will be listed under the Overview section. If you
        have multiple operating systems installed, you can switch between them
        by clicking the Apple icon and selecting System Preferences.
      </p>
      <img
        src="/static/images/MacSysPref.jpg"
        alt="Mac System Preferences"
        className="LsnImgResizer"
      />
      <p>
        Then click Software Update. Your operating system will be listed under
        the Overview section.
      </p>
      <img
        src="/static/images/MacUpdate.jpg"
        alt="Mac Software Update"
        className="LsnImgResizer"
      />
      <p />
      <h4>On Linux:</h4>
      <p>
        Click the Applications icon in the top left corner of your screen. Then
        click System Tools.
      </p>
      <img
        src="/static/images/LinuxSystemtools.jpg"
        alt="Linux System Tools"
        className="LsnImgResizer"
      />
      <p>Click System Information.</p>
      <img
        src="/static/images/LinuxSysInfo.jpg"
        alt="Linux System Information"
        className="LsnImgResizer"
      />
      <p>
        Your operating system will be listed under the Operating System section.
      </p>
    </div>
  );
}

export default Lesson0Step4;

// define Angry
//
