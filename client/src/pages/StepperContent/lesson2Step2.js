//Common types of phishing
function Lesson2Step1() {
  return (
    <div>
      <p>
        Phishing comes in many different forms from email to SMS. They all have
        the same purpose which is to steal your sensitive information or cause
        damage to your systems. It is important to understand what the common
        types are in order to prevent these types of attacks.
      </p>
      <div>
        <b>Spear Phishing</b>
        <p>
          A tailored message aimed at targeting specific people or groups which
          often includes information known to the target. Note in this example
          attention is put in the email in which the company of the receiver is
          mentioned as to specifically target the receiver to click on the
          download link.{" "}
        </p>
      </div>
      <img
        src="/static/images/Spear_Phishing.jpg"
        alt="Spear_Phishing_Img"
        className="LsnImgResizer"
      />
      <div>
        <b>Smishing</b>
        <p>
          The word comes from the combination of “SMS” and phishing, this is a
          phishing attack delivered via SMS and is a popular attack due to the
          fact people are more trusting of a SMS message than an email message.
          Usually, the message will impersonate legitimate organisations,
          individuals or brands. A banking company or government organisation
          are classic examples. In this example the message impersonates a
          legitimate bank, and the message creates a sense of urgency in order
          to trick the recipient to click on the malicious link.{" "}
        </p>
      </div>
      <img
        src="/static/images/Smishing.jpg"
        alt="Smishing_Img"
        className="LsnImgResizer"
      />

      <div>
        <b>Vishing</b>
        <p>
          Vishing is where a phishing message is delivered via a voice call,
          like the smishing, it usually has the attacker impersonate a
          legitimate organisation, individual or brand to make themselves seem
          trusted and trick the recipient to give up important details such as
          bank account information or passwords. An example would be someone
          calling claiming to be a banking employee and they've detected there
          is suspicious usage with your credit card and for it not to be locked
          they'll ask to provide the credit card details. Once you provide these
          details the attacker can make fraudulent charges to the credit card.{" "}
        </p>
      </div>
      <img
        src="/static/images/Vishing.jpg"
        alt="Vishing_Img"
        className="LsnImgResizer"
      />
    </div>
  );
}

export default Lesson2Step1;
