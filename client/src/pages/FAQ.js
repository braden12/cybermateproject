import React from "react";
import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/Forms";
import { Link } from "react-router-dom";

function FAQ() {
  return (
    <div>
      <div className="full-page">
        <NavBar />
        <Wrapper className="FAQBox">
          <div>
            <h2>Common FAQ</h2>
            <p className="subhead">
              Here is some of our most frequently asked questions
            </p>

            <div>
              {" "}
              {/*Question 1*/}
              <h5>
                <b>Q1.Is your product free?</b>
              </h5>
              <p>
                Yes our product is completely free & is available to anyone &
                everyone!
              </p>
            </div>

            <div>
              {" "}
              {/*Question 2*/}
              <h5>
                <b>Q2.What data do you collect?</b>
              </h5>
              <p>
                The full list of what data we collect is on our terms page which
                can be found here:
                <br />
                <Link to="/terms" className="btn">
                  Terms
                </Link>
              </p>
            </div>

            <div>
              {" "}
              {/*Question 3*/}
              <h5>
                <b>
                  Q3. I have found a bug and/or an issue, who can i contact?
                </b>
              </h5>
              <p>
                If you have found a bug or have an issue you wish to report
                please contact us & notify us about the issue so we can fix it
                ASAP via our contact methods below:
                <br />
                <Link to="/contact" className="btn">
                  Contact us
                </Link>
              </p>
            </div>
          </div>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
}

export default FAQ;
