import React from "react";
import LessonsContext from "../context/lessonsContext";
import { useContext } from "react";
import MUIHeaderFunct from "./MUIHeaderFunct";
import "./Dashboard.css";
import Carousel from "../components/Carousel-Component";
import Carousel2 from "../components/Carousel-Component2";
import Carousel3 from "../components/Carousel-Component3";
import MediaCard from "../components/Cards";

function Dashboard() {
  return (
    <div className="FixGap BG">
      <MUIHeaderFunct />

      <div className="margin">
        <div className="section1">
          <h3>Module 1: Threat Landscape</h3>
          <Carousel />
        </div>
        <div className="section2">
          <h3>Module 2: Building Digital Defences</h3>
          <Carousel2 />
        </div>
        <div className="section3">
          <h3>Module 3: How Hackers Think</h3>
          <Carousel3 />
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
