import { useEffect, useState } from "react";
import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/signupPage";
import FormRow from "../components/FormRow";
import { Link } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import Alert from "../components/alert";
import { useNavigate } from "react-router-dom";

import {
  PASSWORD_LENGTH_SHORT,
  PASSWORD_MISSING_CHARACTER,
  PASSWORD_NOT_MATCHING,
  FNAME_LENGTH_SHORT,
  LNAME_LENGTH_SHORT,
  FNAME_LENGTH_LONG,
  LNAME_LENGTH_LONG,
} from "../context/actions.js";

function isAllPresent(str) {
  // Regex to check if a string
  // contains uppercase, lowercase
  // special character & numeric value
  var pattern = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
  );

  // If the string is empty
  // then return false
  if (!str || str.length === 0) {
    return false;
  }

  // Return true if the string matches
  if (pattern.test(str)) {
    return true;
  }
  return false;
}

const initialState = {
  fname: "",
  lname: "",
  email: "",
  password: "",
  password2: "",
};

const Signup = () => {
  // create state values to manipulated
  const [values, setValues] = useState(initialState);

  // create navigation component
  const navigate = useNavigate();

  // get context values and functions
  const { user, showAlert, displayAlert, registerUser, displayPassAlert } =
    useAppContext();

  // updates "values" with given characters
  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  // on submission, check if all fields have values
  const onSubmit = (e) => {
    e.preventDefault();
    const { fname, lname, email, password, password2 } = values;
    if (!fname || !lname || !email || !password || !password2) {
      displayAlert();
      return;
    } else if (fname.trim().length < 2) {
      displayPassAlert(FNAME_LENGTH_SHORT);
      return;
    } else if (fname.trim().length > 20) {
      displayPassAlert(FNAME_LENGTH_LONG);
      return;
    } else if (lname.trim().length < 2) {
      displayPassAlert(LNAME_LENGTH_SHORT);
      return;
    } else if (lname.trim().length > 20) {
      displayPassAlert(LNAME_LENGTH_LONG);
      return;
      // check both passwords are the same
    } else if (password.trim().length < 8 || password2.trim().length < 8) {
      displayPassAlert(PASSWORD_LENGTH_SHORT);
      return;
      // check both passwords are the same
    } else if (password.trim() === password2.trim()) {
      // check passwords have uppercase, lowercase, numeric values and special characters
      if (isAllPresent(password.trim())) {
        const currentUser = { fname, lname, email, password };
        registerUser(currentUser);
      } else {
        displayPassAlert(PASSWORD_MISSING_CHARACTER);
      }
    } else {
      displayPassAlert(PASSWORD_NOT_MATCHING);
    }

    // create a user
    //const currentUser = { fname, lname, email, password };
    //console.log(currentUser);
    // register the user
    //registerUser(currentUser);
  };

  // when "user" is changed, if there is user, navigate to dashboard
  useEffect(() => {
    if (user) {
      setTimeout(() => {
        navigate("/dashboard"); //Navigates to home or where ever you specify after 3 seconds
      }, 3000);
    }
  }, [user, navigate]);

  return (
    <div>
      <div className="PageSUL">
        <NavBar />
        <Wrapper>
          <form className="form" onSubmit={onSubmit}>
            <h3>Sign Up</h3>
            {showAlert && <Alert />}
            {/* name input */}
            <FormRow
              type="text"
              name="fname"
              value={values.fname}
              handleChange={handleChange}
              labelText="First Name"
            />
            <FormRow
              type="text"
              name="lname"
              value={values.lname}
              handleChange={handleChange}
              labelText="Last Name"
            />
            {/* email input */}
            <FormRow
              type="email"
              name="email"
              value={values.email}
              handleChange={handleChange}
              labelText="Email"
            />
            {/* password input */}
            <FormRow
              type="password"
              name="password"
              value={values.password}
              handleChange={handleChange}
              labelText="Password"
            />
            {/* confirm password input */}
            <FormRow
              type="password"
              name="password2"
              value={values.password2}
              handleChange={handleChange}
              labelText="Confirm Password"
            />
            <p>
              By creating an account with us you agree to following:
              <br />
              <Link to="/terms" className="member-btn">
                Terms & Conditions
              </Link>
              <br />
              <Link to="/PrivacyPolicy" className="member-btn">
                Privacy Policy
              </Link>
            </p>
            <button type="submit" className="btn btn-block">
              Submit
            </button>
            <p>
              Already a member?
              <br />
              <Link to="/login" className="member-btn">
                Login{" "}
              </Link>
            </p>
          </form>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
};
export default Signup;
