import { useState } from "react";
import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/forgot";
import FormRow from "../components/FormRow";
import { useParams } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import { useNavigate } from "react-router-dom";
import Alert from "../components/alert";

import {
  PASSWORD_LENGTH_SHORT,
  PASSWORD_MISSING_FIELDS,
  PASSWORD_MISSING_CHARACTER,
  PASSWORD_NOT_MATCHING,
} from "../context/actions.js";

function isAllPresent(str) {
  // Regex to check if a string
  // contains uppercase, lowercase
  // special character & numeric value
  var pattern = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
  );

  // If the string is empty
  // then return false
  if (!str || str.length === 0) {
    return false;
  }

  // Return true if the string matches
  if (pattern.test(str)) {
    return true;
  }
  return false;
}

function ResetPage() {
  const navigate = useNavigate();
  const { activation_token } = useParams();
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const { Reset, showAlert, displayPassAlert } = useAppContext();

  const onSubmit = async (event) => {
    event.preventDefault();

    if (password1.trim().length === 0 || password2.trim().length === 0) {
      // if one field is empty
      displayPassAlert(PASSWORD_MISSING_FIELDS);
      return;
    } else if (password1.trim().length < 8 || password2.trim().length < 8) {
      //if password less than 8 char, display alert
      displayPassAlert(PASSWORD_LENGTH_SHORT);
      return;
    } else if (password1.trim() === password2.trim()) {
      // check both passwords are the same

      // check passwords have uppercase, lowercase, numeric values and special characters
      if (isAllPresent(password1.trim())) {
        // call reset function with password and activation_token as parameter
        const res = await Reset(password1.trim(), activation_token);
        if (res === true) {
          setTimeout(() => {
            navigate("/login"); //Navigates to home or where ever you specify after 3 seconds
          }, 3000);
        } else {
          alert("Error " + res.status + ": Something went wrong");
        }
      } else {
        displayPassAlert(PASSWORD_MISSING_CHARACTER);
      }
    } else {
      displayPassAlert(PASSWORD_NOT_MATCHING);
    }
  };

  return (
    <div>
      <div className="full-page">
        <NavBar />

        <Wrapper>
          <form className="form" onSubmit={onSubmit}>
            <h3>Reset Password:</h3>
            {showAlert && <Alert />}
            <label>
              Enter the new password associated with your account:
              <FormRow
                type="password"
                name="password1"
                value={password1}
                handleChange={(e) => setPassword1(e.target.value)}
                labelText="new password"
              />
              <FormRow
                type="password"
                name="password2"
                value={password2}
                handleChange={(e) => setPassword2(e.target.value)}
                labelText="confirm password"
              />
            </label>
            <button type="submit" className="btn btn-block">
              Submit
            </button>
          </form>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
}
export default ResetPage;
