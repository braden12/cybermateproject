import Signup from "./signup";
import Login from "./login";
import AboutUs from "./about_us";
import Error from "./error";
import Home from "./home";
import Terms from "./terms";
import Contact from "./contact";
import Forgot from "./ForgotPwd";
import Dashboard from "./Dashboard";
import FAQ from "./FAQ";
import MUIHeaderFunct from "./MUIHeaderFunct";
import LoggedInUserRepellant from "./LoggedInUserRepellant";
import ProtectedRoute from "./ProtectedRoute";
import { Lessons, LessonPage } from "./lessons";
import ResetPage from "./ResetPage";

export {
  Signup,
  Login,
  AboutUs,
  Error,
  Home,
  Terms,
  Contact,
  Forgot,
  Dashboard,
  FAQ,
  MUIHeaderFunct,
  LoggedInUserRepellant,
  ProtectedRoute,
  Lessons,
  LessonPage,
  ResetPage,
};
