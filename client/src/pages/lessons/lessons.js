import { Outlet, Link } from "react-router-dom";
import MUIHeaderFunct from "../MUIHeaderFunct";
import "../../pages/Dashboard.css";
import { useContext, useEffect } from "react";
import LessonsContext from "../../context/lessonsContext";
import React from "react";

import Carousel_Component from "../../components/Carousel-Component";

function Lessons() {
  const [schema, setSchema, counter, setCounter] = useContext(LessonsContext); //extract the data provided by lessonsContext
  //console.log(schema.lessons[0]);
  return (
    <div className="FixGap BG">
      <MUIHeaderFunct />
      <div className="margin">
        <div className="section1">
          <h3>Module 1: Threat Landscape</h3>
          <Carousel_Component />
        </div>
        <div className="section1">
          <h3>Module 2: Building Digital Defences</h3>
          <Carousel_Component />
        </div>
        <div className="section1">
          <h3>Module 3: How Hackers Think</h3>
          <Carousel_Component />
        </div>
      </div>
    </div>
  );
}
export default Lessons;
