import "./lesson.css";
import VerticalLinearStepper from "../../components/stepper";
import MUIHeaderFunct from "../MUIHeaderFunct";
import "../../pages/Dashboard.css";
import LessonsContext from "../../context/lessonsContext";
import React, { useContext, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import { height } from "@mui/system";

function LessonPage() {
  const [schema, setSchema, counter, setCounter] = useContext(LessonsContext); //extract the data provided by lessonsContext
  const { moduleID, lessonID } = useParams(); // the parameter from the URL e.g. cybermate.com/lessons/moduleID/lessonID
  let navigate = useNavigate(); // component for navigating to new pages in routes
  var index; // for storing the verified index value for the lesson

  // code to verify the url parameters match what's in the schema
  {
    schema.lessons.map(
      // iterate through the lessons in the schema
      (lesson) =>
        // When the parameters match, collect the id in index
        {
          lesson.urlParam === lessonID && (index = lesson.id);
        }
    );
  }

  // On first load, if index isn't set (no lesson exists with this url param) navigate to the error page
  useEffect(() => {
    !(index || index === 0) && navigate("/error");
  }, []);

  // ensures the counter is the correct value when a new lesson is loaded
  // useEffect(() => {
  //   setCounter(schema.lessons[index].currentIndex);
  //   console.log("LESSON ID CHANGED");
  // }, [lessonID]);

  // changes the step related to which heading in the LHS list is clicked
  const changeStep = (e) => {
    e.preventDefault();
    setCounter(parseInt(e.target.id)); // each heading has an id corresponding to its index in the schema
  };

  // array of indices for all the headings that will be made
  const lessonHeadingArray = schema.lessons[index].lessonContent;

  // change the css for headings based on the active step
  useEffect(() => {
    // for each heading in the array
    lessonHeadingArray.map((heading, index) => {
      var el = document.getElementById(index);

      // if index === counter getElementById and set className underlined
      if (index === counter) {
        el.className = "currentStepHeading";
      } else if (index < counter) {
        el.className = "previousStepHeading";
      } else if (index > counter) {
        el.className = "incompleteStepHeading";
      }
    });
  }, [counter]);

  if (index === 0 || index) {
    // index === 0 for fringe case, as 0 evaluates as false by default
    return (
      <div className="FixGap">
        <MUIHeaderFunct />
        <div className="mainDiv">
          <div className="div1">
            <h3 className="LHSHeading">{schema.lessons[index].title}</h3>

            {/* for each content heading*/}
            {schema.lessons[index].lessonContent.map((content, i) => (
              <div key={content.contentHeading}>
                <h3
                  onClick={changeStep}
                  id={i}
                  className="incompleteStepHeading"
                >
                  {content.contentHeading}
                </h3>
              </div>
            ))}
          </div>

          <div className="div2">
            <h1 className="heading">{schema.lessons[index].title}</h1>
            <VerticalLinearStepper
              lessonID={schema.lessons[index].id}
              urlParam={lessonID}
            />
            {/* Pass through the lesson you want rendered in the stepper */}
          </div>
        </div>
      </div>
    );
  }
}

export default LessonPage;
