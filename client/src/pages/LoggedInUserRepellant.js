import { useAppContext } from "../context/appContext";
import { Navigate } from "react-router-dom";

// used in App.js, ensures only logged out users can access certain pages
const LoggedInUserRepellant = ({ children }) => {
  const { user } = useAppContext();
  if (user) {
    return <Navigate to="/dashboard" />;
  }
  return children;
};

export default LoggedInUserRepellant;
