import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/Forms";
import PrivPolContent from "../components/PrivacyPolicy/PrivPolContent";
function PrivacyPolicy() {
    return (
        <div>
            <div className="terms">
                <NavBar />
                    <Wrapper>
                        <div className="Box">
                            <PrivPolContent/>
                        </div>
                    </Wrapper>
            </div>
            <Footer/>
        </div>
    );
}
export default PrivacyPolicy;