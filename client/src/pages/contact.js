import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/Forms";
function contact() {
  return (
    <div>
      <div className="full-page">
        <NavBar />
        <Wrapper>
          <h2>Contact Us:</h2>
          <p>
            <b>Email: </b>cycademybusiness@gmail.com
            <br />
            <b>Office: </b>25 Northfields Ave, Wollongong NSW 2522
            <br />
          </p>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
}
export default contact;
