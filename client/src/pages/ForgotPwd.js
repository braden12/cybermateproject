import { useState } from "react";
import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/forgot";
import FormRow from "../components/FormRow";
import Alert from "../components/alert";
// import our context
import { useAppContext } from "../context/appContext";
import { FORGOT_USER_EMPTY } from "../context/actions.js";
function Forgot() {
  const { forgot, showAlert, displayPassAlert } = useAppContext();
  const [email, setEmail] = useState("");

  const onSubmit = (event) => {
    event.preventDefault();

    if (email.trim().length !== 0) {
      //alert(`An Email has been sent to: ${email} to reset your password `);
      //console.log(email)
      forgot(email);
    } else {
      displayPassAlert(FORGOT_USER_EMPTY);
    }

    // call our context function with email parameter
  };

  return (
    <div>
      <div className="full-page">
        <NavBar />

        <Wrapper>
          <form className="form" onSubmit={onSubmit}>
            <h3>Forgot Password:</h3>
            {showAlert && <Alert />}
            <label>
              Enter the Email associated with your account:
              <FormRow
                type="email"
                name="email"
                value={email}
                handleChange={(e) => setEmail(e.target.value)}
                labelText="email@example.com"
              />
            </label>
            <button type="submit" className="btn btn-block">
              Submit
            </button>
          </form>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
}
export default Forgot;
