/*jshint eqeqeq: false */

import { useState } from "react";
import MUIHeaderFunct from "./MUIHeaderFunct";
import Wrapper from "../assets/wrappers/updateAccount";
import FormRow from "../components/FormRow";
import Alert from "../components/alert";
// import our context
import { useAppContext } from "../context/appContext";

function UpdateInfo() {
  var user = JSON.parse(localStorage.getItem("user"));
  const token = localStorage.getItem("token");

  const { updateInfo, UpdateEmail, showAlert } = useAppContext();
  var [email, setEmail] = useState("");
  var [lname, setLname] = useState("");
  var [fname, setFname] = useState("");

  const onSubmit = (event) => {
    event.preventDefault();

    if (fname.trim().length == 0) {
      //console.log("fname empty");
      fname = user.fname;
    }
    if (lname.trim().length == 0) {
      //console.log("lname empty");
      lname = user.lname;
    }
    if (email.trim().length == 0) {
      email = user.email;
    } else {
      //console.log("DO something");
      UpdateEmail(email);
      //displayPassAlert(EMAIL_SENT);
    }
    const details = { fname, lname };
    user["fname"] = fname;
    user["lname"] = lname;
    localStorage.setItem("user", JSON.stringify(user));
    //console.log(details, token);

    updateInfo(details, token);
    //displayPassAlert(DETAILS_SAVED);
  };

  return (
    <div className="full-page">
      <div style={{ margin: "-8px", paddingRight: "16px" }}>
        <MUIHeaderFunct />
      </div>
      <Wrapper>
        <form className="form" onSubmit={onSubmit}>
          <h3>Change Account Details:</h3>
          {showAlert && <Alert />}
          <label>
            First Name:
            <FormRow
              type="fname"
              name="fname"
              value={fname}
              handleChange={(e) => setFname(e.target.value)}
              labelText="First Name"
            />
          </label>
          <label>
            Last Name:
            <FormRow
              type="lname"
              name="lname"
              value={lname}
              handleChange={(e) => setLname(e.target.value)}
              labelText="Last Name"
            />
          </label>
          <label>
            New email:
            <FormRow
              type="email"
              name="email"
              value={email}
              handleChange={(e) => setEmail(e.target.value)}
              labelText="email@example.com"
            />
          </label>
          <button type="submit" className="btn btn-block">
            Save
          </button>
        </form>
      </Wrapper>
    </div>
  );
}
export default UpdateInfo;
