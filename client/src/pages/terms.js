import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/Forms";
import TermsContent from "../components/Terms/TermsContent";
function terms() {
  return (
    <div>
      <div className="terms">
        <NavBar />
        <Wrapper>
          <div className="Box">
            <TermsContent/>
          </div>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
}
export default terms;
