import axios from "axios"
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { useAppContext } from "../context/appContext"
import { useNavigate } from "react-router-dom"

function ActivateLayout() {
    const {user, ActivateUser} = useAppContext() // gets the user schema and ActivateUser() function from the appContext
    const { activation_token } = useParams(); // gets activation token from DOM parameters 
    const navigate = useNavigate() // creates a component for navigating routes
    
 
    useEffect(() => {
      // check token exists in ...state (aka. appContext)
      if (activation_token) {
        ActivateUser(activation_token); // function to activate the user
      } 
      
      
      // checks if user from ..state exists
      if(user){
        setTimeout(()=>{navigate("/dashboard") // Navigates to logged in home
        }, 1000) 
      } else {
        setTimeout(()=>{navigate("/") // Navigates to logged out home
        }, 1000) 
      }
      
    });
    
    return (
        <div>
        </div>
      );
};

export default ActivateLayout;