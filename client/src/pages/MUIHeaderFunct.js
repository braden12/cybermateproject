import React from "react";
import MUIHeader from "../components/MUIHeader/MUIHeader";
import { StyledEngineProvider } from "@mui/material/styles";


function MUIHeaderFunct() {
    return (
        <StyledEngineProvider injectFirst>
            <MUIHeader />
        </StyledEngineProvider>
    )
}
export default MUIHeaderFunct; 
