import { useEffect, useState } from "react";
import NavBar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Wrapper from "../assets/wrappers/signupPage";
import FormRow from "../components/FormRow";
import { Link } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import Alert from "../components/alert";
import { useNavigate } from "react-router-dom";

const initialState = {
  email: "",
  password: "",
  isMember: false,
};

const Login = () => {
  const [values, setValues] = useState(initialState);
  const navigate = useNavigate();
  //global state and useNavigate

  const { user, showAlert, displayAlert, loginUser } = useAppContext();

  //Change this function for proper text handling
  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    //console.log(e.target.value)
  };
  const onSubmit = (e) => {
    e.preventDefault();
    //console.log(e.target)
    const { email, password } = values;
    if (!email || !password) {
      displayAlert();
      return;
    }
    const currentUser = { email, password };
    loginUser(currentUser);
  };

  useEffect(() => {
    if (user) {
      setTimeout(() => {
        navigate("/dashboard"); //Navigates to home or where ever you specify. Go into f12 > application > storage > local storage > localhost3000 > clearall
      }, 3000);
    }
  }, [user, navigate]);

  return (
    <div>
      <div className="PageSUL">
        <NavBar />
        <Wrapper>
          <form className="form" onSubmit={onSubmit}>
            <h3>Login</h3>
            {showAlert && <Alert />}
            {/* email input */}
            <FormRow
              type="email"
              name="email"
              value={values.email}
              handleChange={handleChange}
              labelText="Email"
            />
            {/* password input */}
            <FormRow
              type="password"
              name="password"
              value={values.password}
              handleChange={handleChange}
              labelText="Password"
            />
            <button type="submit" className="btn btn-block">
              Submit
            </button>
            <p>
              Not a member yet? &nbsp;
              <Link to="/signup" className="member-btn">
                Sign Up{" "}
              </Link>
              <br />
              <Link to="/forgot" className="member-btn">
                Forgot Password?
              </Link>
            </p>
          </form>
        </Wrapper>
      </div>
      <Footer />
    </div>
  );
};
export default Login;
