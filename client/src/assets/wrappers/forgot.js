import styled from "styled-components";

const Wrapper = styled.section`
  align-items: center;
  flex: 1;
  display: grid;
  border-style: hidden;
  text-align: center;
  justify-content: center;
  color: var(--black);
  margin: 3% 35% 3% 35%;
  border-radius: 25px;
  padding: 3%;

  button {
    margin-top: 5%;
    margin-bottom: 3%;
  }
  input {
    min-width: 75%;
  }
  .container {
    display: block;
    position: absolute;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  .form {
    max-width: 400px;
    border-top: 5px solid var(--primary-500);
  }

  .All {
    background-color: var(--blue-dark);
    color: white;
    margin-left: -0.5%;
    margin-right: -0.4%;
    margin-top: 0.7%;
  }

  /* Hide the browser's default checkbox */
  .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  /* Create a custom checkbox */
  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
  }

  /* On mouse-over, add a grey background color */
  .container:hover input ~ .checkmark {
    background-color: #ccc;
  }

  /* When the checkbox is checked, add a blue background */
  .container input:checked ~ .checkmark {
    background-color: #2196f3;
  }

  /* Create the checkmark/indicator (hidden when not checked) */
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }

  /* Show the checkmark when checked */
  .container input:checked ~ .checkmark:after {
    display: block;
  }

  /* Style the checkmark/indicator */
  .container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;
export default Wrapper;
