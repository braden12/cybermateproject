import styled from "styled-components";

const Form = styled.section`
  display: grid;
  border-style: hidden;
  text-align: center;
  justify-content: center;
  color: var(--black);
  background-color: var(--white);
  margin: 5% 32% 3% 32%;
  padding: 1.8% 3% 1.8% 3%;
  border-radius: 25px;
  ul {
    display: list-item;
  }
  h4 {
    font-size: 120%;
  }
  h5 {
    font-size: 100%;
    text-align: left;
  }
  h3 {
    text-align: left;
  }
  .subhead {
    color: var(--grey-500);
  }
  p {
    font-size: 20px;
  }
`;
export default Form;
