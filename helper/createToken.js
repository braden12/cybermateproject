import jwt from 'jsonwebtoken'

//called in authController to create the email authentication token 
const createToken = {
  activation: (payload) => {
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "1h" });
  }
};

export default createToken